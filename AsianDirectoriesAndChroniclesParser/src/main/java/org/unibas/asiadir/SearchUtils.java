package org.unibas.asiadir;

public class SearchUtils {

	public static boolean isNumeric(String value) {
		try {
			Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

	public static boolean matchesOneSearchCriteria(String nodeValue, String... searchValues) {
		for(String value : searchValues) {
			if(value.equals(nodeValue)) {
				return true;
			}
		}
		return false;
	}
	
	public static String replaceAll(String source, String pattern, String replacement) {
        if (source == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        int index;
        int patIndex = 0;
        while ((index = source.indexOf(pattern, patIndex)) != -1) {
            sb.append(source.substring(patIndex, index));
            sb.append(replacement);
            patIndex = index + pattern.length();
        }
        sb.append(source.substring(patIndex));
        return sb.toString();
    }
}
