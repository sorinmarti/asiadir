package org.unibas.asiadir.narafileparser;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.List;

import org.unibas.asiadir.FileHandler;

public class PrisonersOfWarParser {

	private static CodeTranslator gradeAlphaCT       = new CodeTranslator("cl_1275.csv");
	private static CodeTranslator gradeCodeCT        = new CodeTranslator("cl_1279.csv");
	private static CodeTranslator serviceCodeCT      = new CodeTranslator("cl_1278.csv");
	private static CodeTranslator armOrServiceCT     = new CodeTranslator("cl_1270.csv");
	private static CodeTranslator armOrServiceCodeCT = new CodeTranslator("cl_1269.csv");
	private static CodeTranslator dateYearTranslator = new CodeTranslator("cl_1606.csv");
	private static CodeTranslator racialGroupCT      = new CodeTranslator("cl_1268.csv");
	private static CodeTranslator stateOfResidenceCT = new CodeTranslator("cl_1271.csv");
	private static CodeTranslator typeOfOrgCT        = new CodeTranslator("cl_1959.csv");
	private static CodeTranslator parentUnitTypeCT   = new CodeTranslator("cl_1960.csv");
	private static CodeTranslator areaCT             = new CodeTranslator("cl_1273.csv");
	private static CodeTranslator sourceOfReportCT   = new CodeTranslator("cl_1281.csv");
	private static CodeTranslator statusCT           = new CodeTranslator("cl_1280.csv");
	private static CodeTranslator detainingPowerCT   = new CodeTranslator("cl_1272.csv");
	private static CodeTranslator campCT             = new CodeTranslator("cl_1277.csv");
	private static CodeTranslator powTransportShipsCT= new CodeTranslator("cl_1276.csv");
	
	private static String DEL = ";";	// Column delimiter
	
	private static String csvStringTitle = "Serial Number"+DEL+
			   							   "Name"+DEL+
			   							   "Grade, Alpha"+DEL+
			   							   "Grade Code"+DEL+
			   							   "Service Code"+DEL+
			   							   "Arm or Service"+DEL+
			   							   "Arm or Service Code"+DEL+
			   							   "Date Report"+DEL+
			   							   "Racial Group"+DEL+
			   							   "State of Residence"+DEL+
			   							   "Type of Organization"+DEL+
			   							   "Parent Unit Number"+DEL+
			   							   "Parent Unit Type"+DEL+
			   							   "Area"+DEL+
			   							   "Latest Report Date"+DEL+
			   							   "Source of Report"+DEL+
			   							   "Status"+DEL+
			   							   "Detaining Power"+DEL+
			   							   "Camp"+DEL+
			   							   "Rep"+DEL+
			   							   "Pow Transport Ships"+"\n";
	
	public static String csvStringTitleShort = "Serial Number"+DEL+
											   "Name"+DEL+
											   "Grade, Alpha"+DEL+
											   "Service Code"+DEL+
											   "Arm or Service"+DEL+
											   "Date Report"+DEL+
											   "Racial Group"+DEL+
											   "Area"+DEL+
											   "Latest Report Date"+DEL+
											   "Status"+DEL+
											   "Detaining Power"+DEL+
											   "Camp"+DEL+
											   "Pow Transport Ships"+"\n";
	
	public static void main(String[] args) throws IOException {
		File file = FileHandler.getFileFromResources("/POW.MERGED");
		List<String> allLines = Files.readAllLines(file.toPath());
		
		int numLines = allLines.size();
		int lineToProcess = 0;
		int splitFileNumber = 0;
		
		// Full version
		String csvString = csvStringTitle;
		
		// Short version
		String csvStringShort = csvStringTitleShort;
		
		for(String line : allLines) {
			lineToProcess++;
			
			String serial 			= line.substring( 0,  8);
			String name 			= line.substring( 8, 32);
			
			String gradeAlpha 		= getClearTextValue(line.substring(32, 38), gradeAlphaCT);
			String gradeCode 		= getClearTextValue(line.substring(38, 41), gradeCodeCT);
			String serviceCode 		= getClearTextValue(line.substring(41, 42), serviceCodeCT);
			String armOrService 	= getClearTextValue(line.substring(42, 45), armOrServiceCT);
			String armOrServiceCode = getClearTextValue(line.substring(45, 47), armOrServiceCodeCT);
			
			String dateDay 			= line.substring(47, 49);
			String dateMonth 		= line.substring(49, 51);
			String dateYear 		= getClearTextValue(line.substring(51, 52), dateYearTranslator);
			
			String racialGroup 		= getClearTextValue(line.substring(52, 53), racialGroupCT);
			String stateOfResidence = getClearTextValue(line.substring(53, 55), stateOfResidenceCT);
			String typeOfOrg 		= getClearTextValue(line.substring(55, 58), typeOfOrgCT);
			String parentUnitNumber = line.substring(58, 62);
			String parentUnitType 	= getClearTextValue(line.substring(62, 64), parentUnitTypeCT);
			String area 			= getClearTextValue(line.substring(64, 66), areaCT);
			String latestDay 		= line.substring(66, 68);
			String latestMonth 		= line.substring(68, 70);
			String latestYear 		= getClearTextValue(line.substring(70, 71), dateYearTranslator);
			String sourceOfReport 	= getClearTextValue(line.substring(71, 72), sourceOfReportCT);
			String status 			= getClearTextValue(line.substring(72, 73), statusCT);
			String detainingPower 	= getClearTextValue(line.substring(73, 74), detainingPowerCT);
			String camp 			= getClearTextValue(line.substring(74, 77), campCT);
			String rep 				= line.substring(77, 78);
			String powTransportShips= getClearTextValue(line.substring(78, 80), powTransportShipsCT);
			
			String csvLine = serial+DEL+
		 			  		 name+DEL+
		 			  		 gradeAlpha+DEL+
		 			  		 gradeCode+DEL+
		 			  		 serviceCode+DEL+
		 			  		 armOrService+DEL+
		 			  		 armOrServiceCode+DEL+
		 			  		 dateDay+"."+dateMonth+"."+dateYear+DEL+
		 			  		 racialGroup+DEL+
		 			  		 stateOfResidence+DEL+
		 			  		 typeOfOrg+DEL+
		 			  		 parentUnitNumber+DEL+
		 			  		 parentUnitType+DEL+
		 			  		 area+DEL+
		 			  		 latestDay+"."+latestMonth+"."+latestYear+DEL+
		 			  		 sourceOfReport+DEL+
		 			  		 status+DEL+
		 			  		 detainingPower+DEL+
		 			  		 camp+DEL+
		 			  		 rep+DEL+
		 			  		 powTransportShips+"\n";
			
			String csvLineShort = serial+DEL+
					 		 	  name+DEL+
					 		 	  gradeAlpha+DEL+
					 		 	  serviceCode+DEL+
					 		 	  armOrService+DEL+
					 		 	  dateDay+"."+dateMonth+"."+dateYear+DEL+
					 		 	  racialGroup+DEL+
					 		 	  area+DEL+
					 		 	  latestDay+"."+latestMonth+"."+latestYear+DEL+
					 		 	  status+DEL+
					 		 	  detainingPower+DEL+
					 		 	  camp+DEL+
					 		 	  powTransportShips+"\n";
			
			csvString += csvLine;
			csvStringShort += csvLineShort;
			
			// Console output to indicate that the script is running
			if(lineToProcess%1000==0) {
				System.out.println("Line "+String.format("%1$6s", lineToProcess) + " of " + numLines + ": done.");
			}
			
			if(lineToProcess%50000==0) {
				splitFileNumber++;
				try (PrintWriter out = new PrintWriter("pow_list_full_"+splitFileNumber+".csv")) {
				    out.println(csvString);
				    csvString = csvStringTitle;
				}
				try (PrintWriter out = new PrintWriter("pow_list_short_"+splitFileNumber+".csv")) {
				    out.println(csvStringShort);
				    csvStringShort = csvStringTitleShort;
				}
			}
		}
		
		// Write out the remaining stuff
		splitFileNumber++;
		try (PrintWriter out = new PrintWriter("pow_list_full_"+splitFileNumber+".csv")) {
		    out.println(csvString);
		    csvString = csvStringTitle;
		}
		try (PrintWriter out = new PrintWriter("pow_list_short_"+splitFileNumber+".csv")) {
		    out.println(csvStringShort);
		    csvStringShort = csvStringTitleShort;
		}
	}

	private static String getClearTextValue(String code, CodeTranslator translator) {
		String value = translator.getClearTextFromCode(code.trim());
		if(value==null) {
			return "";
		}
		return value;
	}
}
