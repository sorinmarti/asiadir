package org.unibas.asiadir.narafileparser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.unibas.asiadir.FileHandler;

public class CodeTranslator {

	private Map<String, String> keyValues = new HashMap<String, String>();
	
	public CodeTranslator(String fileName) {
		File file = FileHandler.getFileFromResources("/code_translators/"+fileName);
		List<String> allLines;
		try {
			allLines = Files.readAllLines(file.toPath());
			for(String line : allLines) {
				String[] parts = line.split(",");
				parts[0] = parts[0].replaceAll("\"", "");
				parts[1] = parts[1].replaceAll("\"", "");
				keyValues.put(parts[0], parts[1]);
			}
			
		} catch (IOException e) {
			System.err.println("Could not load code translator: "+fileName);
		}
		
		
	}

	public String getClearTextFromCode(String code) {
		return keyValues.get(code);
	}
	
}
