package org.unibas.asiadir.archreader;

import java.util.ArrayList;
import java.util.List;

public class ResultPage {

	List<ResultObject> resultsOnPage;
	String nextLink;
	
	public ResultPage(String nextLink) {
		this.nextLink = nextLink;
		resultsOnPage = new ArrayList<>();
	}
	
	public void addResult(ResultObject obj) {
		resultsOnPage.add(obj);
	}
	
	public String getNextLink() {
		return nextLink;
	}

	public void setNextLink(String nextLink) {
		this.nextLink = nextLink;
	}

	public List<ResultObject> getResultObjects() {
		return resultsOnPage;
	}
}
