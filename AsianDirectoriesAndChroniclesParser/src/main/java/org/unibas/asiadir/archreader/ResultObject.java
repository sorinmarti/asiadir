package org.unibas.asiadir.archreader;

import java.util.ArrayList;
import java.util.List;

public class ResultObject {

	private String title;
	private String location;
	private String linkStub;
	private List<DetailCollection> detailCollections;
	
	public ResultObject() {
		title = "";
		location = "";
		linkStub = "";
		detailCollections = new ArrayList<>();
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getLinkStub() {
		return linkStub;
	}
	
	public void setLinkStub(String linkStub) {
		this.linkStub = linkStub;
	}
	
	@Override
	public String toString() {
		return "ResultObject [title=" + title + ", location=" + location + ", linkStub=" + linkStub + ", details=" + detailCollections.size() +"]";
	}
	
	public String getDetailCSVString() {
		String csv = "";
		for(DetailCollection dc : detailCollections) {
			csv += ";"+dc.getDescription()+";"+dc.getRepository()+";"+NationalArchivesPageCrawler.baseURL+dc.getLinkStub();
		}
		return csv;
	}
	
	public boolean filterMatchesTitle() {
		for(String word : NationalArchivesPageCrawler.matchingWords) {
			if(title!=null && title.toLowerCase().contains(word)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean filterMatchesLocation() {
		for(String word : NationalArchivesPageCrawler.matchingWords) {
			if(location!=null && location.toLowerCase().contains(word)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean filterMatchesAddedCompanyName() {
		for(String word : NationalArchivesPageCrawler.addedCompanies) {
			if(title!=null && title.toLowerCase().contains(word)) {
				return true;
			}
		}
		return false;
	}

	public void addDetailCollection(DetailCollection dc) {
		detailCollections.add(dc);
	}
	
}
