package org.unibas.asiadir.archreader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public class NationalArchivesPageCrawler {

	static String baseURL = "https://discovery.nationalarchives.gov.uk";
	
	public static void main(String[] args) throws FileNotFoundException {
		
		System.out.println("STARTING SEARCH");
		
		String csvString = "Company Name;Location;Details Link;Matching Decision;"
						 + "Collection 1: Description;Collection 1: Archive;Collection 1: Detail link;"
						 + "Collection 2: Description;Collection 2: Archive;Collection 2: Detail link;"
						 + "Collection 3: Description;Collection 3: Archive;Collection 3: Detail link;"
						 + "Collection 4: Description;Collection 4: Archive;Collection 4: Detail link;"
						 + "\n";
		String next = "/browse/c/business/F180441/first";
		
		int pagesChecked = 1;
		int resultNumber = 0;
		while(next!=null) {
			pagesChecked++;
			System.out.println("Checking "+next);
			ResultPage result =  checkPage(baseURL+next);
			
			for(ResultObject resObj : result.getResultObjects()) {
				int matching = 0;
				if(resObj.filterMatchesTitle()) {
					matching += 1;
				}
				if(resObj.filterMatchesLocation()) {
					matching += 2;
				}
				if(resObj.filterMatchesAddedCompanyName()) {
					matching += 4;
				}
				
				
				if(matching>0) {		// There is a match
					// Now get the details from the details page
					readDetailsPage(resObj);
					
					//TODO add details to csv string
					csvString += resObj.getTitle()+";"+resObj.getLocation()+";"+baseURL+resObj.getLinkStub()+";"+matchingDesicion(matching)+resObj.getDetailCSVString()+"\n";
					
					// Test Printing to console 
					resultNumber++;
					System.out.println(resultNumber+": "+resObj.toString());
				}
					
			}
			next = result.getNextLink();
			//System.out.println("-----------------------------------------------");
		}
		
		System.out.println("SEARCH ENDED");
		System.out.println("--------------------------------");
		System.out.println("Pages Searched:  "+pagesChecked);
		System.out.println("Entries Checked: "+(pagesChecked*30));
		System.out.println("Entries Found:   "+resultNumber);
		System.out.println(csvString);
		
		try (PrintWriter out = new PrintWriter("crawlingResult.csv")) {
		    out.println(csvString);
		}
		
		try (PrintWriter out = new PrintWriter("crawlingStats.csv")) {
			String statsString = "";
			statsString += "Pages Searched;"+pagesChecked+"\n";
			statsString += "Entries Checked;"+(pagesChecked*30)+"\n";
			statsString += "Entries Found;"+resultNumber+"\n";
			statsString += "Title and Location Keywords;"+matchingWords[0]+"\n";
			for(int i=1;i<matchingWords.length;i++) {
				statsString += ";"+matchingWords[i]+"\n";
			}
			statsString += "Company Name Keywords;"+addedCompanies[0]+"\n";
			for(int i=1;i<addedCompanies.length;i++) {
				statsString += ";"+addedCompanies[i]+"\n";
			}
		    out.println(statsString);
		}
		
	}
	
	private static void readDetailsPage(ResultObject resObj) {
		String contents = "";
		try {
			contents = readStringFromURL(baseURL+resObj.getLinkStub());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		String[] collectionsResults = StringUtils.substringsBetween(contents, "<tr class=\"collection-row\">", "</tr>");
		for(String colResult : collectionsResults) {
			// Isolates the table cell, but there's divs within
			String descriptionContainer = StringUtils.substringBetween(colResult, "<td class=\"description\"", "</td>");
			String repositoryContainer = StringUtils.substringBetween(colResult, "<td class=\"repository\">", "</td>");
			
			// Strips containers from html data
			String description = "";
			String linkStub = "";
			String repository = "";
			
			if(descriptionContainer!=null) {
				description = StringUtils.substringBetween(descriptionContainer, "<div class=\"cell-wrap restrict-text\">", "</div>");
				linkStub = StringUtils.substringBetween(descriptionContainer, "<a href=\"", "\">");
			}
			if(repositoryContainer!=null) {
				repository = StringUtils.substringBetween(repositoryContainer, "<div class=\"cell-wrap\">", "</div>");
			}
			
			if(description==null) {
				System.out.println("Description crawling for page failed");
			}
					
			// description always starts with newline (0d, 0a)
			if(description !=null && description.length()>2) {
				description = description.substring(2);
			}
			
			DetailCollection dc = new DetailCollection();
			dc.setDescription(description);
			dc.setRepository(repository);
			dc.setLinkStub(linkStub);
			resObj.addDetailCollection(dc);
		}
	}

	private static String matchingDesicion(int matching) {
		switch(matching) {
		case 0:
			return "No match";
		case 1:
			return "Title";
		case 2:
			return "Location";
		case 3:
			return "Title & Location";
		case 4:
			return "Company Name";
		case 5:
			return "Company name & Title";
		case 6:
			return "Company name & Location";
		case 7:
			return "Company name, Title & Location";
		}
		
		return "Undefined";
	}

	private static ResultPage checkPage(String url) {
		String contents = "";
		try {
			contents = readStringFromURL(url);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		String nextLink = StringUtils.substringBetween(contents, "<a class=\"next\" href=\"", "\">Next 30</a>");
		ResultPage page = new ResultPage(nextLink);		
		String[] blockResults = StringUtils.substringsBetween(contents, "<a class=\"linkTitle\"", "</a>");
		
		for(String result : blockResults) {
			String linkResult     = StringUtils.substringBetween(result, "href=\"", "\"");
			String titleResult    = StringUtils.substringBetween(result, "<h3>", "</h3>");
			String locationResult = StringUtils.substringBetween(result, "<span title=\"", "\" class=\"itemDescription\"");
			
			//System.out.println(linkResult);
			//System.out.println(titleResult);
			//System.out.println(locationResult);
			ResultObject resObj = new ResultObject();
			resObj.setTitle(titleResult);
			resObj.setLocation(locationResult);
			resObj.setLinkStub(linkResult);
			page.addResult(resObj);
		}
		
		return page;
	}

	private static boolean hasKeyWords(String result) {
		String temp = result.toLowerCase();
		if(temp.contains("china") ||
		   temp.contains("japan") ||
		   temp.contains("malaya")) {
			return true;
		}
		return false;
	}

	public static String readStringFromURL(String requestURL) throws IOException {
	    try (Scanner scanner = new Scanner(new URL(requestURL).openStream(), StandardCharsets.UTF_8.toString())) {
	        scanner.useDelimiter("\\A");
	        return scanner.hasNext() ? scanner.next() : "";
	    }
	}
	
	public static String[] matchingWords = {
			"singapore",
			"hong kong",
			"singapore",
			"yokohama",
			"canton",
			"yangtze",
			"shanghai",
			"manila",
			"manilla",
			"china",
			"chinese",
			" chine ",			// finds machine, machinery: spaces prevent this
			"japan",
			"asia",
			"australasian",
			"far east",
			"borneo",
			"north borneo",
			"new guinea",
			"oriental",
			"colonial"};
	
	public static String[] addedCompanies = {
			"Berrick",
			"Bickerton",
			"Birchal",
			"Birley",
			"Boyd",
			"Butterfield",
			"Swire",
			"Carlill",
			"Chapman",
			"Cornes",
			"Dick",
			"Kerr",
			"Dodwell",
			"Drysdale",
			"Wilkinson",
			"Elles",
			"Etti",
			"Evans",
			"Pugh",
			"Finch",
			"Findlay",
			"Forbes",
			"Fraser", 
			"Farley",
			"Geen", 
			"Evison", 
			"Stutchbury",
			"Gibb", 
			"Livinston",
			"Gilman",
			"Gittins",
			"Griffith",
			"Hall",
			"Goodwin",
			"Harvie",
			"Milne",
			"Hewett",
			"Holliday",
			"Wise",
			"Hunt",
			"Iveson",
			"Jardine",
			"Matheson",
			"Johnston",
			"Kilby",
			"Kingdon",
			"Lank",
			"Crawford",
			"Maitland",
			"Newman",
			"Odell",
			"Leyburn",
			"Oliver",
			"Phipps",
			"Marians", 
			"Bethell", 
			"Moss",
			"Remington",
			"Rooke",
			"Russel",
			"Samuel",
			"Harding",
			"Strachan",
			"Strauss",
			"Rawlins",
			"Turnbull", 
			"Howie"};
}
