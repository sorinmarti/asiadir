package org.unibas.asiadir.archreader;

public class DetailCollection {

	private String description;
	private String repository;
	private String linkStub;
	
	public DetailCollection() {
		description = "";
		repository = "";
		linkStub = "";
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getRepository() {
		return repository;
	}
	
	public void setRepository(String repository) {
		this.repository = repository;
	}
	
	public String getLinkStub() {
		return linkStub;
	}
	
	public void setLinkStub(String linkStub) {
		this.linkStub = linkStub;
	}
	
	
	
}
