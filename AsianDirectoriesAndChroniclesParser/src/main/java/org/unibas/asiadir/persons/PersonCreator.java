package org.unibas.asiadir.persons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class PersonCreator {

	protected static void addPersonsToPlaceList(File file, List<Place> places) throws IOException {
		String row;
		int notFoundOccurence = 0;
		BufferedReader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
		
		// SEARCH THROUGH PERSONS AND ADD THEM TO PLACES
		while ((row = csvReader.readLine()) != null) {    
	    	String[] data = row.split("\t");
	    	// If at least a preferred label is set:
	    	// we compute the entry
	    	if(data.length>=4) {
	    		Person person = new Person();
	    		person.setName(data[0]);
	    		person.setName(data[1]);
	    		person.setName(data[3]);
	    		Place place = PlaceCreator.searchForPlace(places,data[2].trim());
	    		if(place!=null) {
	    			place.addPerson(person);
	    			AsiaDirParser.print("--> ADDED [person] '' to '"+place.getPreferredLabel()+"'");
	    		}
	    		else {
	    			notFoundOccurence++;
	    			AsiaDirParser.print("--> NOT FOUND [place] from variation '"+data[2]+"' ("+notFoundOccurence+")");
	    		}
	    	}
	    	else {
	    		AsiaDirParser.print("--> IGNORED [person] b/c input line was malformed: '"+row+"'");
	    	}
		    
		}
		csvReader.close();
	}
	
	protected static void createCSVStringFromData(File file, List<Place> places) throws FileNotFoundException, IOException {
		//String output = "id;name;lat;long;value";
		String output = "";
		int id = 1;
		for(Place p : places) {
			if(p.getLatitude()!=null && p.getLongitude()!=null && !p.getLatitude().isEmpty() && !p.getLongitude().isEmpty() && p.getPersons().size()>0 ) {
				output += id + ";" + p.getPreferredLabel() + ";" + p.getLatitude() + ";" + p.getLongitude() + ";" + p.getPersons().size() + "\n";
				id++;
			}
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
			 writer.write(output);
			 writer.close();
		}
	}
}
