package org.unibas.asiadir.persons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.graph.Triple;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.lang.PipedRDFIterator;
import org.apache.jena.riot.lang.PipedRDFStream;
import org.apache.jena.riot.lang.PipedTriplesStream;
import org.unibas.asiadir.jena.TurtleFileByIDParser;

public class PlaceCreator {

	public static List<Place> getPlacesWithVariationsFromTSV(File file) throws IOException {
		
		List<Place> places = new ArrayList<Place>();
		
		String row;
		int line = 0;
		BufferedReader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8));
		//BufferedReader csvReader = new BufferedReader(new InputStreamReader(new FileInputStream(FileHandler.getFileFromResources("/normalized_placenames.tsv")), StandardCharsets.UTF_8));
		//BufferedReader csvReader = new BufferedReader(new FileReader(FileHandler.getFileFromResources("/normalized_placenames.tsv")));
		while ((row = csvReader.readLine()) != null) {
		    if(line>0) {
		    	String[] data = row.split("\t");
		    	// If at least a preferred label is set:
		    	// we compute the entry
		    	if(data.length>=5) {
		    		Place place = new Place(data[4]);
		    		place.setTgnNumber(data[5]);
		    		place.setLatitude(data[7]);
		    		place.setLongitude(data[8]);
		    		
		    		place.setReviewComment(data[2]);
		    		if(!data[2].isEmpty()) {
		    			place.setUncertain(true);
		    		}
		    		
		    		for(int i=9;i<40;i++) {
		    			if(i<data.length) {
		    				place.addNameVariation(data[i]);
		    			}
		    		}
		    		places.add(place);
		    		AsiaDirParser.print("--> FOUND [place] '"+place.getPreferredLabel()+"'");
		    	}
		    	else {
		    		AsiaDirParser.print("--> IGNORED [place] b/c input line was malformed: '"+row+"'");
		    	}
		    }
		    
		    line++;
		}
		csvReader.close();
		
		AsiaDirParser.print("--> PARSED [places] "+places.size()+" places found.");		
		return places;
	}

	public static Place searchForPlace(List<Place> places, String variation) {
		for(Place p : places) {
			if(p.getNameVariations().contains(variation)) {
				return p;
			}
		}
		return null;
	}

	public static void updateGeoData(List<Place> places) {
		for(Place p : places) {
			if(!p.getTgnNumber().isEmpty() && (p.getLatitude().isEmpty() || p.getLongitude().isEmpty())) {
				try {
					System.out.println("Fetching coordinates for: "+p.getPreferredLabel() + "("+p.getTgnNumber()+")");
					String[] latLong = getCoordinatesFromTGN(p.getTgnNumber());
					p.setLatitude(latLong[0]);
					p.setLongitude(latLong[1]);
					AsiaDirParser.print("--> FETCHED [geodata] for '"+p.getPreferredLabel()+"': ("+p.getLongitude()+"/"+p.getLatitude()+")");
				} catch (URISyntaxException | IOException e) {
					AsiaDirParser.print("--> FETCH FAILED [geodata] for '"+p.getPreferredLabel()+"', error: "+e.getMessage());
				}
			}
		}
	}
	
	private static String[] getCoordinatesFromTGN(String tgn) throws URISyntaxException, IOException {
		final String fileUrl = "http://vocab.getty.edu/tgn/"+tgn+".ttl";
		final String fileName = tgn+".ttl";
		
		URL website = new URL(fileUrl);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		String dir = TurtleFileByIDParser.class.getResource("/").getFile();
		FileOutputStream fos = new FileOutputStream(dir + "/" + fileName);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
		
		PipedRDFIterator<Triple> iter = new PipedRDFIterator<>();
		final PipedRDFStream<Triple> inputStream = new PipedTriplesStream(iter);

		RDFDataMgr.parse(inputStream, dir + "/" + fileName, RDFLanguages.TTL);

		// We can now iterate over data as it is parsed, parsing only runs as
		// far ahead of our consumption as the buffer size allows
		String longitude = null;
		String latitude = null;
		while (iter.hasNext()) {
			Triple next = iter.next();
	            
			if(next.getPredicate().toString().endsWith("longitude")) {
				longitude = next.getObject().getIndexingValue().toString();
			}
			if(next.getPredicate().toString().endsWith("latitude")) {
				latitude = next.getObject().getIndexingValue().toString();
			}
		}
	    
		return new String[]{longitude, latitude};
	}

	public static void savePlaceList(File file, List<Place> places, boolean printId) throws IOException {
		int idx = 1;
		String csvString = "";
		for(Place p : places) {
			if(printId) {
				csvString += p.getCSVLine(idx) + "\n";
			}
			else {
				csvString += p.getCSVLine() + "\n";
			}
			idx++;
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8)) {
			writer.write(csvString);
			writer.close();
		}
	}
}
