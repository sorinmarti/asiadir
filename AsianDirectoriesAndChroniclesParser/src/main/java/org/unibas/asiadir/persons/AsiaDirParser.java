package org.unibas.asiadir.persons;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class AsiaDirParser {

	public static void main(String[] args) throws IOException {
		if(args.length==0 || args[0].equals("help")) {
			printHelp();
			System.exit(0);
		}
		
		String functionToExecute = args[0];
		
		switch(functionToExecute) {
		case "updatePlaceList":
			executeUpdatePlaceList(args);
			System.exit(0);
			break;
		case "createPersonsList":
			executeCreatePersonsList(args);
			System.exit(0);
			break;
		default:
			System.out.println("Command not recognized. Use 'help' for options.");
			System.exit(0);
		}
	}
	
	private static void executeCreatePersonsList(String[] args) throws IOException {
		// Check if params are valid
		if(args.length!=4) {
			print("Command malformed. Use 'help' for options.");
			System.exit(-1);
		}
		// "Welcome" message
		//createPersonsList <placeListFileName.tsv> <personsList.tsv> <resultFileName>
		print("About to create the persons list...");
		print(" -place input file:  "+args[1]);
		print(" -person input file: "+args[2]);
		print(" -output file:       "+args[3]);
		
		// Check if input files exists
		String placeInputFileName = args[1];
		File placeInputFile = new File(placeInputFileName);
		if(!placeInputFile.exists() || !placeInputFile.canRead()) {
			print("Place input file does not exist or is not readable.");
			System.exit(-1);
		}
		String personInputFileName = args[2];
		File personInputFile = new File(personInputFileName);
		if(!personInputFile.exists() || !personInputFile.canRead()) {
			print("Person input file does not exist or is not readable.");
			System.exit(-1);
		}
		
		// Check if output file can be created
		String outputFileName = args[3];
		File outputFile = new File(outputFileName);
		//if(outputFile.exists() || !outputFile.canWrite()) {
		if(outputFile.exists()) {
			print("Output file does already exist or is not writable.");
			System.exit(-1);
		}
		
		// Inputs is valid: executeCreatePersonsList
		List<Place> places = PlaceCreator.getPlacesWithVariationsFromTSV(placeInputFile);
		PersonCreator.addPersonsToPlaceList(personInputFile, places);
		PersonCreator.createCSVStringFromData(outputFile, places);
	}

	private static void executeUpdatePlaceList(String[] args) throws IOException {
		// Check if params are valid
		if(args.length!=3) {
			print("Command malformed. Use 'help' for options.");
			System.exit(-1);
		}
		// "Welcome" message
		print("About to update the place list...");
		print(" -input file:  "+args[1]);
		print(" -output file: "+args[2]);
		
		// Check if input file exists
		String inputFileName = args[1];
		File inputFile = new File(inputFileName);
		if(!inputFile.exists() || !inputFile.canRead()) {
			print("Input file does not exist or is not readable.");
			System.exit(-1);
		}
		
		// Check if output file can be created
		String outputFileName = args[2];
		File outputFile = new File(outputFileName);
		if(outputFile.exists() || !outputFile.canWrite()) {
			print("Output file does already exist or is not writable.");
			System.exit(-1);
		}
		
		// Inputs is valid: executeUpdatePlaceList
		print("Parsing place information...");
		List<Place> places = PlaceCreator.getPlacesWithVariationsFromTSV(inputFile);
		print("Updating geo data...");
		PlaceCreator.updateGeoData( places );
		print("Save result to output file...");
		PlaceCreator.savePlaceList( outputFile, places , true);
		print("DONE.");
	}
	
	protected static void print(String string) {
		System.out.println(string);
	}
	
	protected static void print() {
		System.out.println();
	}
	
	private static void printHelp() {
		print("EIB Place And Person Parser");
		print("---------------------------");
		print("Usage: function [param1 param2] ");
		print();
		print("Functions:");
		print("  help");
		print("      --> shows this help text");
		print();
		print("  updatePlaceList <oldPlaceListFilename.tsv> <oldPlaceListFilename.tsv>");
		print("      --> updates geodata of the normalized place list");
		print();
		print("  createPersonsList <placeListFileName.tsv> <personsList.tsv> <resultFileName>");
		print("      --> creates TSV of places with number of persons in each place");
		print();
	}
}
