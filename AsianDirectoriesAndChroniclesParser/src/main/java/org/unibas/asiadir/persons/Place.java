package org.unibas.asiadir.persons;

import java.util.ArrayList;
import java.util.List;

public class Place {

	private final String preferredLabel;
	
	private String tgnNumber;
	private String longitude;
	private String latitude;
	
	private List<String> nameVariations;
	private List<Person> persons;
	
	private String reviewComment;
	private boolean uncertain;
	
	public Place(String preferred) {
		preferredLabel = preferred;
		nameVariations = new ArrayList<String>();
		persons = new ArrayList<Person>();
		reviewComment = "";
		uncertain = false;
	}

	public String getCSVLine(int lineIdx) {
		return getCSVLine(String.valueOf(lineIdx));
	}
	
	public String getCSVLine() {
		return getCSVLine("");
	}

	private String getCSVLine(String lineIdx) {
		String separator = "\t";
		String retString = "";
		if(!lineIdx.isEmpty()) {
			retString += lineIdx+separator;
		}
		retString += preferredLabel + separator +
					 tgnNumber + separator +
					 longitude + separator +
					 latitude + separator +
					 reviewComment + separator +
					 uncertain + separator;
		for(String var : nameVariations) {
			retString += var + separator;
		}
		retString = retString.substring(0, retString.length()-separator.length());
		return retString;
	}

	public String getTgnNumber() {
		return tgnNumber;
	}

	public void setTgnNumber(String tgnNumber) {
		this.tgnNumber = tgnNumber;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<String> getNameVariations() {
		return nameVariations;
	}

	public void addNameVariation(String nameVariation) {
		this.nameVariations.add(nameVariation);
	}
	
	public List<Person> getPersons() {
		return persons;
	}

	public void addPerson(Person person) {
		this.persons.add(person);
	}

	public String getPreferredLabel() {
		return preferredLabel;
	}

	public String getReviewComment() {
		return reviewComment;
	}

	public void setReviewComment(String reviewComment) {
		this.reviewComment = reviewComment;
	}

	public boolean isUncertain() {
		return uncertain;
	}

	public void setUncertain(boolean uncertain) {
		this.uncertain = uncertain;
	}
}
