package org.unibas.asiadir.jena;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.graph.Triple;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.lang.PipedRDFIterator;
import org.apache.jena.riot.lang.PipedRDFStream;
import org.apache.jena.riot.lang.PipedTriplesStream;
import org.unibas.asiadir.FileHandler;

public class TurtleFileByIDParser {

	public static void main(String[] args) throws URISyntaxException, IOException {
		//String link = getCoordinateLink("1000958", "Pipapo");
		//System.out.println(link);
		
		
		try (BufferedReader br = new BufferedReader(new FileReader(FileHandler.getFileFromResources("/test3.csv")))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       try {
		    	   String link = getCoordinateLink(line, "");
		    	   System.out.println(link);
		    	   /*
		    	   String[] parts = line.split(";");
		    	   if(parts.length>1) {
		    		   Integer tgn = Integer.valueOf(parts[1]);
		    		   if(tgn!=0) {
		    			   String link = getCoordinateLink(String.valueOf(tgn), parts[0]);
		    			   System.out.println(link);
		    		   }
		    		   else {
		    			   System.out.println(parts[0]+"\t"+"no tgn is 0");
		    		   }
		    	   }
		    	   else {
		    		   System.out.println(parts[0]+"\t"+"no tgn supplied");
		    	   }
		    	   */
		       } catch (NumberFormatException e) {
		    	   e.printStackTrace();
		    	   System.out.println("NaN: "+line);
			}
		    	
		    }
		}
		
	}
	
	public static String getCoordinateLink(String tgn, String origName) throws URISyntaxException, IOException {
		final String fileUrl = "http://vocab.getty.edu/tgn/"+tgn+".ttl";
		final String fileName = tgn+".ttl";
		
		URL website = new URL(fileUrl);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		String dir = TurtleFileByIDParser.class.getResource("/").getFile();
		FileOutputStream fos = new FileOutputStream(dir + "/" + fileName);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
		
		PipedRDFIterator<Triple> iter = new PipedRDFIterator<>();
		final PipedRDFStream<Triple> inputStream = new PipedTriplesStream(iter);

		RDFDataMgr.parse(inputStream, dir + "/" + fileName, RDFLanguages.TTL);

		// We can now iterate over data as it is parsed, parsing only runs as
		// far ahead of our consumption as the buffer size allows
		String longitude = null;
		String latitude = null;
		String parentString = "";
		String description = "";
		String placeType = null;
		String placeTypeString = null;
		List<String> names = new ArrayList<>();
		while (iter.hasNext()) {
			Triple next = iter.next();
	            
			if(next.getPredicate().toString().endsWith("longitude")) {
				longitude = next.getObject().getIndexingValue().toString();
			}
			if(next.getPredicate().toString().endsWith("latitude")) {
				latitude = next.getObject().getIndexingValue().toString();
			}
			if(next.getPredicate().toString().endsWith("parentString")) {
				parentString = next.getObject().getIndexingValue().toString();
			}
			
			if(next.getPredicate().toString().endsWith("core#prefLabel")) {
				//System.out.println( next.getPredicate() + " \n " + next.getObject()+ " \n " + next.getSubject() + " \n ");
				String lbl = next.getObject().getIndexingValue().toString();
				String[] parts = lbl.split("@");
				names.add(parts[0]);
			}
			
			if(next.getPredicate().toString().endsWith("22-rdf-syntax-ns#value")) {
				description = next.getObject().getIndexingValue().toString();
			}
			
			if(next.getPredicate().toString().contains("placeTypePreferred")) {
				//description = next.getObject().getIndexingValue().toString();
				//System.out.println( next.getPredicate() + " \n " + next.getObject()+ " \n " + next.getSubject() + " \n ");
				placeType = next.getObject().toString();
			}
			else {
				if(placeType==null) {
					placeType = "No preferred PlaceType";
				}
			}
			//gvp:placeTypePreferred aat:300265612 ;
			
		}
	    
		String prefLabelsString = "";
		for(String n : names) {
			prefLabelsString += n+",";
		}
		
		placeTypeString = translatePlaceTypeString(placeType);
		placeType = translatePlaceType(placeType);
		
		prefLabelsString = prefLabelsString.substring(0, prefLabelsString.length()-1);
		return tgn+"\t\t"+placeType+"\t"+placeTypeString;
		//return origName+"\t"+tgn+"\t"+latitude+"\t"+longitude;
		//return latitude;
		//return tgn+";"+origName+";"+prefLabelsString+";"+parentString+";"+latitude+";"+longitude+";https://www.google.com/maps/place/"+latitude+"+"+longitude+";"+description;
	}

	private static String translatePlaceTypeString(String placeType) {
		if(placeType==null) {
			return "no type specified";
		}
		String[] typeSplit = placeType.split("/");
		String lastPart = typeSplit[typeSplit.length-1]; 
		return "http://www.getty.edu/vow/AATFullDisplay?find=&logic=AND&note=&subjectid="+lastPart;
	}

	private static String translatePlaceType(String placeType) {
		if(placeType==null) {
			return "no type specified";
		}
		String[] typeSplit = placeType.split("/");
		String lastPart = typeSplit[typeSplit.length-1]; 
		String placeTypeName = "undefined ("+lastPart+")";
		switch(lastPart) {
		case "300008347":
			placeTypeName = "Inhabited place";						// 1
			break;
		case "300387178":
			placeTypeName = "Historical region";					// 2
			break;
		case "300000776":
			placeTypeName = "State (political division)";			// 3
			break;
		case "300128207":
			placeTypeName = "Nation";								// 4
			break;
		case "300000774":
			placeTypeName = "Province";								// 5
			break;
		case "300412029":
			placeTypeName = "Former territory/Colony/Dependent state";	// 6
			break;
		case "300000745":
			placeTypeName = "Neighborhood";							// 7
			break;
		case "300008804":
			placeTypeName = "Peninsula";							// 8
			break;
		case "300182722":
			placeTypeName = "Region (geographic)";					// 9
			break;
		case "300387123":
			placeTypeName = "Federal territory";					// 10
			break;
		case "300387080":
			placeTypeName = "Autonomous district";					// 11
			break;
		case "300387052":
			placeTypeName = "Semi-independent political entity";	// 12
			break;
		case "300235099":
			placeTypeName = "Prefecture";							// 13
			break;
		case "300008791":
			placeTypeName = "Island (landform)";					// 14
			break;
		case "300387346":
			placeTypeName = "General region";						// 15
			break;
		}
		
		return placeTypeName; 
	}
}
