package org.unibas.asiadir;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class FileHandler {

	public static File getFileFromResources(String fileName) {
		File file = null;
		try {
			 URL fileURL = FileHandler.class.getResource(fileName);			 
			 file = new File(fileURL.toURI());
		 } catch (Exception e) {
			return null;
		 }
		return file;
	}
	
	public static Document getDocumentFromFile(File file) {
		if(file==null) {
			return null;
		}
		
		Document doc = null;
		try {
			DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			doc = dBuilder.parse(file);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return doc;
	}

}
