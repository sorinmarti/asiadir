package org.unibas.asiadir.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.IssueEntry;
import org.unibas.asiadir.db.entries.NationEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;
import org.unibas.asiadir.db.entries.TownEntry;

import net.miginfocom.swing.MigLayout;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	private JTextField textField_1;
	
	// Companies
	private JList<CompanyEntry> listCompanies;
	private JTextField txtCompanyId;
	private JTextArea txtCompanyName;
	
	// N-Companies
	private JList<NormalizedCompanyEntry> listNCompanies;
	private JTextField txtNCompanyId;
	private JTextField txtNCompanyName;
	private JComboBox<NationEntry> cmbNCompanyNation;
	private JList<NormalizedAgencyEntry> listAgencyOfCompany;
	private JList<NationEntry> listNationsOfCompany;
	private JList<CompanyEntry> listRecordsOfCompany;
	
	// Agencies
	private JList<AgencyEntry> listAgencies;
	private JTextField txtAgencyId;
	private JTextField txtAgencyName;
	
	// N-Agencies
	private JList<NormalizedAgencyEntry> listNAgencies;
	private JTextField txtNAgencyId;
	private JTextField txtNAgencyName;
	private JComboBox<NationEntry> cmbNAgencyNationality;
	private JList<AgencyEntry> listNAgencyAgencies;
	
	// Nations
	private JList<NationEntry> listNations;
	private JTextField txtNationId;
	private JTextField txtNationName;
	
	// Towns
	private JList<TownEntry> listTowns;
	private JTextField txtTownId;
	private JTextField txtTownName;
	private JComboBox<NationEntry> cmbTownNation;
	
	// Issues
	private JList<IssueEntry> listIssues;
	private JTextField txtIssueId;
	private JTextField txtIssueName;
	private JTextField txtIssuePages;
	private JTextField txtCompanyIssue;
	private JTextField txtCompanyPagePdf;
	private JTextField txtCompanyState;
	private JTextField txtCompanyType;
	private JTextField txtCompanyPageSrc;
	private JTextField txtCompanyRepresents;
	
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1190, 604);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel pnlCompanies = new JPanel();
		tabbedPane.addTab("Company Records", null, pnlCompanies, null);
		pnlCompanies.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListCompanies = new JPanel();
		pnlListCompanies.setBorder(new TitledBorder(null, "Saved Companies", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCompanies.add(pnlListCompanies, BorderLayout.WEST);
		pnlListCompanies.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		pnlListCompanies.add(panel_7, BorderLayout.SOUTH);
		
		listCompanies = new JList<>();
		listCompanies.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				CompanyEntry e = listCompanies.getSelectedValue();
				txtCompanyId.setText(String.valueOf(e.getId()));
				txtCompanyName.setText(e.getName());
			}
		});
		
		JScrollPane scrollPane_3 = new JScrollPane(listCompanies);
		scrollPane_3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListCompanies.add(scrollPane_3, BorderLayout.CENTER);
		
		JPanel pnlDetailCompanies = new JPanel();
		pnlDetailCompanies.setBorder(new TitledBorder(null, "Company Detail", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCompanies.add(pnlDetailCompanies, BorderLayout.CENTER);
		pnlDetailCompanies.setLayout(new MigLayout("", "[][grow][grow][grow][grow][grow]", "[][][grow][]"));
		
		JLabel lblId_5 = new JLabel("ID");
		pnlDetailCompanies.add(lblId_5, "cell 0 0,alignx trailing");
		
		txtCompanyId = new JTextField();
		pnlDetailCompanies.add(txtCompanyId, "cell 1 0,growx");
		txtCompanyId.setColumns(10);
		
		JLabel lblType = new JLabel("TYPE");
		pnlDetailCompanies.add(lblType, "cell 2 0,alignx trailing");
		
		txtCompanyType = new JTextField();
		pnlDetailCompanies.add(txtCompanyType, "cell 3 0,growx");
		txtCompanyType.setColumns(10);
		
		JLabel lblState = new JLabel("STATE");
		pnlDetailCompanies.add(lblState, "cell 4 0,alignx trailing");
		
		txtCompanyState = new JTextField();
		pnlDetailCompanies.add(txtCompanyState, "cell 5 0,growx");
		txtCompanyState.setColumns(10);
		
		JLabel lblLo = new JLabel("ISSUE");
		pnlDetailCompanies.add(lblLo, "cell 0 1,alignx right");
		
		txtCompanyIssue = new JTextField();
		pnlDetailCompanies.add(txtCompanyIssue, "cell 1 1,growx");
		txtCompanyIssue.setColumns(10);
		
		JLabel lblPagesrcpdf = new JLabel("PAGE (SRC)");
		pnlDetailCompanies.add(lblPagesrcpdf, "cell 2 1,alignx trailing");
		
		txtCompanyPageSrc = new JTextField();
		pnlDetailCompanies.add(txtCompanyPageSrc, "cell 3 1,growx");
		txtCompanyPageSrc.setColumns(10);
		
		JLabel lblPagepdf = new JLabel("PAGE (PDF)");
		pnlDetailCompanies.add(lblPagepdf, "cell 4 1,alignx trailing");
		
		txtCompanyPagePdf = new JTextField();
		pnlDetailCompanies.add(txtCompanyPagePdf, "cell 5 1,growx");
		txtCompanyPagePdf.setColumns(10);
		
		JLabel lblName_5 = new JLabel("TEXT");
		pnlDetailCompanies.add(lblName_5, "cell 0 2,alignx trailing,aligny top");
		
		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_7.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlDetailCompanies.add(scrollPane_7, "cell 1 2 5 1,grow");
		
		txtCompanyName = new JTextArea();
		scrollPane_7.setViewportView(txtCompanyName);
		txtCompanyName.setColumns(10);
		
		JLabel lblEntries_1 = new JLabel("REPRESENTS");
		pnlDetailCompanies.add(lblEntries_1, "cell 0 3,alignx trailing");
		
		txtCompanyRepresents = new JTextField();
		pnlDetailCompanies.add(txtCompanyRepresents, "cell 1 3 5 1,growx");
		txtCompanyRepresents.setColumns(10);
		
		JPanel pnlNCompanies = new JPanel();
		tabbedPane.addTab("N-Companies", null, pnlNCompanies, null);
		pnlNCompanies.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListNCompanies = new JPanel();
		pnlListNCompanies.setBorder(new TitledBorder(null, "Saved N-Companies", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlNCompanies.add(pnlListNCompanies, BorderLayout.WEST);
		pnlListNCompanies.setLayout(new BorderLayout(0, 0));
		
		listNCompanies = new JList<>();
		listNCompanies.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				NormalizedCompanyEntry coEntry = listNCompanies.getSelectedValue();
				txtNCompanyId.setText(String.valueOf(coEntry.getId()));
				txtNCompanyName.setText(coEntry.getName());
				cmbNCompanyNation.setSelectedItem(DatabaseService.getInstance().getNationById(coEntry.getNationality()));
				
				// A nCompany ID is supplied
				// To get the represented nAgencies, we need to
				// -> get all the company records of the nCompany
				Set<CompanyEntry> companyRecords = DatabaseService.getInstance().getCompanyEntriesOfCompany(coEntry.getId());
				listRecordsOfCompany.setListData(companyRecords.toArray(new CompanyEntry[companyRecords.size()]));
				
				Set<AgencyEntry> agencyRecordsOfAllCompanyRecords = new HashSet<>();
				for(CompanyEntry companyRecord : companyRecords) {
					Set<AgencyEntry> agencyRecordsOfCompanyRecord = DatabaseService.getInstance().getAgencyRecordsOfCompanyRecord(companyRecord.getId());
					agencyRecordsOfAllCompanyRecords.addAll(agencyRecordsOfCompanyRecord);
				}
				
				Set<NormalizedAgencyEntry> agsOfCompany = new HashSet<>();
				for(AgencyEntry agencyRecord : agencyRecordsOfAllCompanyRecords) {
					NormalizedAgencyEntry agencyOfAgencyRecord = DatabaseService.getInstance().getAgenciesOfAgencyRecord(agencyRecord.getRepresents());
					agsOfCompany.add(agencyOfAgencyRecord);
				}
				listAgencyOfCompany.setListData(agsOfCompany.toArray(new NormalizedAgencyEntry[agsOfCompany.size()]));
				
				Set<NationEntry> nationsOfCompany = new HashSet<>();
				for(NormalizedAgencyEntry agency : agsOfCompany) {
					nationsOfCompany.add(DatabaseService.getInstance().getNationById(agency.getNationality()));
				}
				listNationsOfCompany.setListData(nationsOfCompany.toArray(new NationEntry[nationsOfCompany.size()]));
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(listNCompanies);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListNCompanies.add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel_3 = new JPanel();
		pnlListNCompanies.add(panel_3, BorderLayout.SOUTH);
		
		JPanel pnlDetailNCompanies = new JPanel();
		pnlNCompanies.add(pnlDetailNCompanies, BorderLayout.CENTER);
		pnlDetailNCompanies.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlDetailNCompanies.setLayout(new MigLayout("", "[][grow][grow][grow]", "[][][][][][grow][grow][]"));
		
		JLabel lblId_6 = new JLabel("ID");
		pnlDetailNCompanies.add(lblId_6, "cell 0 0,alignx trailing");
		
		txtNCompanyId = new JTextField();
		pnlDetailNCompanies.add(txtNCompanyId, "cell 1 0,growx,aligny top");
		txtNCompanyId.setColumns(10);
		
		JLabel lblName_6 = new JLabel("NAME");
		pnlDetailNCompanies.add(lblName_6, "cell 0 1,alignx trailing");
		
		txtNCompanyName = new JTextField();
		txtNCompanyName.setColumns(10);
		pnlDetailNCompanies.add(txtNCompanyName, "cell 1 1,growx");
		
		JLabel lblNationality = new JLabel("NATIONALITY");
		pnlDetailNCompanies.add(lblNationality, "cell 0 2,alignx trailing");
		
		cmbNCompanyNation = new JComboBox<NationEntry>();
		pnlDetailNCompanies.add(cmbNCompanyNation, "cell 1 2,growx");
		
		JLabel lblPresentInTowns = new JLabel("Represented Agencies");
		pnlDetailNCompanies.add(lblPresentInTowns, "cell 1 4");
		
		JLabel lblRepresentedAgencies = new JLabel("Represented Nations");
		pnlDetailNCompanies.add(lblRepresentedAgencies, "cell 2 4");
		
		JLabel lblOccurences = new JLabel("Occurences");
		pnlDetailNCompanies.add(lblOccurences, "cell 3 4");
		
		JLabel lblEntries_2 = new JLabel("ENTRIES");
		pnlDetailNCompanies.add(lblEntries_2, "cell 0 5,alignx right");
		
		listAgencyOfCompany = new JList<>();
		JScrollPane scrollPane_8 = new JScrollPane(listAgencyOfCompany);
		scrollPane_8.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_8.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlDetailNCompanies.add(scrollPane_8, "cell 1 5,grow");
		
		listNationsOfCompany = new JList<>();
		JScrollPane scrollPane_9 = new JScrollPane(listNationsOfCompany);
		scrollPane_9.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_9.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlDetailNCompanies.add(scrollPane_9, "cell 2 5,grow");
		
		listRecordsOfCompany = new JList<>();
		JScrollPane scrollPane_10 = new JScrollPane(listRecordsOfCompany);
		scrollPane_10.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_10.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlDetailNCompanies.add(scrollPane_10, "cell 3 5,grow");
		
		JLabel lblStats = new JLabel("STATS");
		pnlDetailNCompanies.add(lblStats, "cell 0 6,alignx right");
		
		JPanel panel_8 = new JPanel();
		pnlDetailNCompanies.add(panel_8, "cell 1 6 3 1,grow");
		panel_8.setLayout(new MigLayout("", "[]", "[][][]"));
		
		JLabel lblNumberOfRepresented = new JLabel("Number of represented agencies:");
		panel_8.add(lblNumberOfRepresented, "cell 0 0,alignx right");
		
		JLabel lblNumberOfRepresented_1 = new JLabel("Number of represented nations:");
		panel_8.add(lblNumberOfRepresented_1, "cell 0 1,alignx right");
		
		JLabel lblNumberOfOccurences = new JLabel("Number of occurences:");
		panel_8.add(lblNumberOfOccurences, "cell 0 2,alignx right");
		
		JLabel lbltools = new JLabel("\u00A8TOOLS");
		pnlDetailNCompanies.add(lbltools, "cell 0 7,alignx trailing");
		
		JPanel panel_5 = new JPanel();
		pnlDetailNCompanies.add(panel_5, "cell 1 7 3 1,grow");
		panel_5.setLayout(new MigLayout("", "[][]", "[]"));
		
		textField_1 = new JTextField();
		panel_5.add(textField_1, "cell 0 0");
		textField_1.setColumns(10);
		
		JButton btnSearchTheInternet = new JButton("Search the internet");
		panel_5.add(btnSearchTheInternet, "cell 1 0");
		
		JPanel pnlAgencies = new JPanel();
		tabbedPane.addTab("Agency Records", null, pnlAgencies, null);
		pnlAgencies.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListAgencies = new JPanel();
		pnlListAgencies.setBorder(new TitledBorder(null, "Saved Agencies", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlAgencies.add(pnlListAgencies, BorderLayout.WEST);
		pnlListAgencies.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_10 = new JPanel();
		pnlListAgencies.add(panel_10, BorderLayout.SOUTH);
		
		listAgencies = new JList<>();
		listAgencies.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				AgencyEntry e = listAgencies.getSelectedValue();
				txtAgencyId.setText(String.valueOf(e.getId()));
				txtAgencyName.setText(e.getName());
			}
		});
		JScrollPane scrollPane_4 = new JScrollPane(listAgencies);
		scrollPane_4.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListAgencies.add(scrollPane_4, BorderLayout.CENTER);
		
		JPanel pnlDetailAgencies = new JPanel();
		pnlDetailAgencies.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Saved Agency Details", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlAgencies.add(pnlDetailAgencies, BorderLayout.CENTER);
		pnlDetailAgencies.setLayout(new MigLayout("", "[][grow]", "[][]"));
		
		JLabel lblId_4 = new JLabel("ID");
		pnlDetailAgencies.add(lblId_4, "cell 0 0,alignx trailing");
		
		txtAgencyId = new JTextField();
		pnlDetailAgencies.add(txtAgencyId, "cell 1 0,growx");
		txtAgencyId.setColumns(10);
		
		JLabel lblName_4 = new JLabel("NAME");
		pnlDetailAgencies.add(lblName_4, "cell 0 1,alignx trailing");
		
		txtAgencyName = new JTextField();
		pnlDetailAgencies.add(txtAgencyName, "cell 1 1,growx");
		txtAgencyName.setColumns(10);
		
		JPanel pnlNAgencies = new JPanel();
		tabbedPane.addTab("N-Agencies", null, pnlNAgencies, null);
		pnlNAgencies.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListNAgencies = new JPanel();
		pnlListNAgencies.setBorder(new TitledBorder(null, "Saved N-Agencies", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlNAgencies.add(pnlListNAgencies, BorderLayout.WEST);
		pnlListNAgencies.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		pnlListNAgencies.add(panel_1, BorderLayout.SOUTH);
		
		listNAgencies = new JList<>();
		listNAgencies.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				NormalizedAgencyEntry e = listNAgencies.getSelectedValue();
				txtNAgencyId.setText(String.valueOf(e.getId()));
				txtNAgencyName.setText(e.getName());
				cmbNAgencyNationality.setSelectedItem(DatabaseService.getInstance().getNationById(e.getNationality()));
				Set<AgencyEntry> entries = DatabaseService.getInstance().getAgencyRecordsOfNAgency(e.getId());
				List<AgencyEntry> targetList = new ArrayList<>(entries);
				Collections.sort(targetList);
				listNAgencyAgencies.setListData(targetList.toArray(new AgencyEntry[targetList.size()]));
			}
		});
		JScrollPane scrollPane_5 = new JScrollPane(listNAgencies);
		scrollPane_5.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListNAgencies.add(scrollPane_5, BorderLayout.CENTER);
		
		JPanel pnlDetailNAgencies = new JPanel();
		pnlDetailNAgencies.setBorder(new TitledBorder(null, "N-Agency Details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlNAgencies.add(pnlDetailNAgencies, BorderLayout.CENTER);
		pnlDetailNAgencies.setLayout(new MigLayout("", "[][grow]", "[][][][grow]"));
		
		JLabel lblId_3 = new JLabel("ID");
		pnlDetailNAgencies.add(lblId_3, "cell 0 0,alignx trailing");
		
		txtNAgencyId = new JTextField();
		pnlDetailNAgencies.add(txtNAgencyId, "cell 1 0,growx");
		txtNAgencyId.setColumns(10);
		
		JLabel lblName_3 = new JLabel("NAME");
		pnlDetailNAgencies.add(lblName_3, "cell 0 1,alignx trailing");
		
		txtNAgencyName = new JTextField();
		pnlDetailNAgencies.add(txtNAgencyName, "cell 1 1,growx,aligny top");
		txtNAgencyName.setColumns(10);
		
		JLabel lblNationality_1 = new JLabel("NATIONALITY");
		pnlDetailNAgencies.add(lblNationality_1, "cell 0 2,alignx trailing");
		
		cmbNAgencyNationality = new JComboBox<NationEntry>();
		pnlDetailNAgencies.add(cmbNAgencyNationality, "cell 1 2,growx");
		
		JLabel lblEntries = new JLabel("ENTRIES");
		pnlDetailNAgencies.add(lblEntries, "cell 0 3");
		
		JScrollPane scrollPane_11 = new JScrollPane();
		pnlDetailNAgencies.add(scrollPane_11, "cell 1 3,grow");
		
		listNAgencyAgencies = new JList<AgencyEntry>();
		scrollPane_11.setViewportView(listNAgencyAgencies);
		
		JPanel pnlNations = new JPanel();
		tabbedPane.addTab("Nations", null, pnlNations, null);
		pnlNations.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListNations = new JPanel();
		pnlListNations.setBorder(new TitledBorder(null, "Saved Nations", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlNations.add(pnlListNations, BorderLayout.WEST);
		pnlListNations.setLayout(new BorderLayout(0, 0));
		
		listNations = new JList<>();
		listNations.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				NationEntry e = listNations.getSelectedValue();
				txtNationId.setText(String.valueOf(e.getId()));
				txtNationName.setText(e.getName());
			}
		});
		JScrollPane scrollPane_1 = new JScrollPane(listNations);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListNations.add(scrollPane_1, BorderLayout.CENTER);
		
		JPanel panel_6 = new JPanel();
		pnlListNations.add(panel_6, BorderLayout.SOUTH);
		
		JPanel pnlDetailNations = new JPanel();
		pnlDetailNations.setBorder(new TitledBorder(null, "Selected Entry", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlNations.add(pnlDetailNations, BorderLayout.CENTER);
		pnlDetailNations.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		JLabel lblId = new JLabel("ID");
		pnlDetailNations.add(lblId, "cell 0 0,alignx trailing");
		
		txtNationId = new JTextField();
		pnlDetailNations.add(txtNationId, "cell 1 0,growx");
		txtNationId.setColumns(10);
		
		JLabel lblName = new JLabel("NAME");
		pnlDetailNations.add(lblName, "cell 0 1,alignx trailing");
		
		txtNationName = new JTextField();
		pnlDetailNations.add(txtNationName, "cell 1 1,growx");
		txtNationName.setColumns(10);
		
		JPanel pnlTowns = new JPanel();
		tabbedPane.addTab("Towns", null, pnlTowns, null);
		pnlTowns.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListTowns = new JPanel();
		pnlListTowns.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Saved Towns", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pnlTowns.add(pnlListTowns, BorderLayout.WEST);
		pnlListTowns.setLayout(new BorderLayout(0, 0));
		
		listTowns = new JList<>();
		listTowns.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				TownEntry e = listTowns.getSelectedValue();
				txtTownId.setText(String.valueOf(e.getId()));
				txtTownName.setText(e.getName());
				cmbTownNation.setSelectedItem(DatabaseService.getInstance().getNationById(e.getCountryId()));
				
			}
		});
		JScrollPane scrollPane_2 = new JScrollPane(listTowns);
		scrollPane_2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnlListTowns.add(scrollPane_2, BorderLayout.CENTER);
		
		JPanel panel_4 = new JPanel();
		pnlListTowns.add(panel_4, BorderLayout.SOUTH);
		
		JPanel pnlDetailTowns = new JPanel();
		pnlDetailTowns.setBorder(new TitledBorder(null, "Town Details", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlTowns.add(pnlDetailTowns, BorderLayout.CENTER);
		pnlDetailTowns.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		JLabel lblId_1 = new JLabel("ID");
		pnlDetailTowns.add(lblId_1, "cell 0 0,alignx trailing");
		
		txtTownId = new JTextField();
		pnlDetailTowns.add(txtTownId, "cell 1 0,growx");
		txtTownId.setColumns(10);
		
		JLabel lblName_1 = new JLabel("NAME");
		pnlDetailTowns.add(lblName_1, "cell 0 1,alignx trailing");
		
		txtTownName = new JTextField();
		pnlDetailTowns.add(txtTownName, "cell 1 1,growx");
		txtTownName.setColumns(10);
		
		JLabel lblCountry = new JLabel("COUNTRY");
		pnlDetailTowns.add(lblCountry, "cell 0 2,alignx trailing");
		
		cmbTownNation = new JComboBox<NationEntry>();
		pnlDetailTowns.add(cmbTownNation, "cell 1 2,growx");
		
		JPanel pnlIssues = new JPanel();
		tabbedPane.addTab("Issues", null, pnlIssues, null);
		pnlIssues.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlListIssues = new JPanel();
		pnlListIssues.setBorder(new TitledBorder(null, "Saved Issues", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlIssues.add(pnlListIssues, BorderLayout.WEST);
		pnlListIssues.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		pnlListIssues.add(panel, BorderLayout.SOUTH);
		
		listIssues = new JList<>();
		listIssues.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				IssueEntry e = listIssues.getSelectedValue();
				txtIssueId.setText(String.valueOf(e.getId()));
				txtIssueName.setText(e.getName());
				txtIssuePages.setText(String.valueOf(e.getPages()));
			}
		});
		JScrollPane scrollPane_6 = new JScrollPane(listIssues);
		scrollPane_6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane_6.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlListIssues.add(scrollPane_6, BorderLayout.CENTER);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Issue Detail", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlIssues.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		JLabel lblId_2 = new JLabel("ID");
		panel_2.add(lblId_2, "cell 0 0,alignx trailing");
		
		txtIssueId = new JTextField();
		panel_2.add(txtIssueId, "cell 1 0,growx");
		txtIssueId.setColumns(10);
		
		JLabel lblName_2 = new JLabel("NAME");
		panel_2.add(lblName_2, "cell 0 1,alignx trailing");
		
		txtIssueName = new JTextField();
		panel_2.add(txtIssueName, "cell 1 1,growx");
		txtIssueName.setColumns(10);
		
		JLabel lblPages = new JLabel("PAGES");
		panel_2.add(lblPages, "cell 0 2,alignx trailing");
		
		txtIssuePages = new JTextField();
		panel_2.add(txtIssuePages, "cell 1 2,growx");
		txtIssuePages.setColumns(10);
		
		JPanel panel_9 = new JPanel();
		tabbedPane.addTab("Settings", null, panel_9, null);
		panel_9.setLayout(new MigLayout("", "[]", "[][][][][][][]"));
		
		JButton button = new JButton("Update Company List");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateCompanyData();
			}
		});
		panel_9.add(button, "cell 0 0,growx");
		
		JButton button_1 = new JButton("Update N-Company List");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNCompanyData();
			}
		});
		panel_9.add(button_1, "cell 0 1,growx");
		
		JButton button_2 = new JButton("Update Agencies List");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateAgencyData();
			}
		});
		panel_9.add(button_2, "cell 0 2,growx");
		
		JButton button_3 = new JButton("Update N-Agency List");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNAgencyData();
			}
		});
		panel_9.add(button_3, "cell 0 3,growx");
		
		JButton button_4 = new JButton("Update Nation List");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateNationData();
			}
		});
		panel_9.add(button_4, "cell 0 4,growx");
		
		JButton button_5 = new JButton("Update Town List");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateTownData();
			}
		});
		panel_9.add(button_5, "cell 0 5,growx");
		
		JButton button_6 = new JButton("Update Issue List");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateIssueData();
			}
		});
		panel_9.add(button_6, "cell 0 6,growx");
		
		try {
			updateAllData();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			JOptionPane.showMessageDialog(this, "Could not update data from database.");
		}
	}
	
	private void updateAllData() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		updateCompanyData();
		updateNCompanyData();
		updateAgencyData();
		updateNAgencyData();
		updateNationData();
		updateTownData();
		updateIssueData();
	}

	private void updateCompanyData() {
		Set<CompanyEntry> entries = DatabaseService.getInstance().getCompanyEntries();
		listCompanies.setListData(entries.toArray(new CompanyEntry[entries.size()]));
	}
	
	private void updateNCompanyData() {
		Set<NormalizedCompanyEntry> entries = DatabaseService.getInstance().getNCompanyEntries();
		listNCompanies.setListData(entries.toArray(new NormalizedCompanyEntry[entries.size()]));
	}
	
	private void updateAgencyData() {
		Set<AgencyEntry> entries = DatabaseService.getInstance().getAgencyEntries();
		listAgencies.setListData(entries.toArray(new AgencyEntry[entries.size()]));
	}
	
	private void updateNAgencyData() {
		Set<NormalizedAgencyEntry> entries = DatabaseService.getInstance().getNAgencyEntries();
		List<NormalizedAgencyEntry> targetList = new ArrayList<>(entries);
		Collections.sort(targetList);
		
		// TODO SORTED DOES NOT BELONG HERE!
		List<NormalizedAgencyEntry> sorted = new ArrayList<>();
		for(NormalizedAgencyEntry nae : targetList) {
			if(nae.getNationality()==6) {
				sorted.add(nae);
			}
		}
		
		listNAgencies.setListData(sorted.toArray(new NormalizedAgencyEntry[sorted.size()]));
	}
	
	private void updateNationData() {
		Set<NationEntry> entries = DatabaseService.getInstance().getNationEntries();
		listNations.setListData(entries.toArray(new NationEntry[entries.size()]));
		cmbNCompanyNation.setModel(new DefaultComboBoxModel<NationEntry>(entries.toArray(new NationEntry[entries.size()])));
		cmbTownNation.setModel(new DefaultComboBoxModel<NationEntry>(entries.toArray(new NationEntry[entries.size()])));
		cmbNAgencyNationality.setModel(new DefaultComboBoxModel<NationEntry>(entries.toArray(new NationEntry[entries.size()])));
	}
	
	private void updateTownData() {
		Set<TownEntry> entries = DatabaseService.getInstance().getTownEntries();
		listTowns.setListData(entries.toArray(new TownEntry[entries.size()]));
	}

	private void updateIssueData() {
		Set<IssueEntry> entries = DatabaseService.getInstance().getIssueEntries();
		listIssues.setListData(entries.toArray(new IssueEntry[entries.size()]));
	}
}
