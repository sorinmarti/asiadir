package org.unibas.asiadir.db.agencies;

import java.sql.SQLException;
import java.util.List;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;

public class MainClassAgencies {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		AgencyGroupingForm form = new AgencyGroupingForm();
		form.setVisible(true);
		
	}

	public static void createNormalizedAgencyRecord(String text) throws ClassNotFoundException, SQLException {
		NormalizedAgencyEntry newEntry = new NormalizedAgencyEntry();
		newEntry.setName(text);
		DatabaseService.getInstance().createNormalizedAgencyEntry(newEntry);
	}

	public static void updateAgencyEntries(int normalizedAgencyId, List<AgencyEntry> entries) throws ClassNotFoundException, SQLException {
		for(AgencyEntry agency : entries) {
			agency.setRepresents(normalizedAgencyId);
			DatabaseService.getInstance().updateAgencyEntry(agency);
			System.out.println("Would update: "+agency.getName());
		}
		System.err.println("updateAgencyEntries");
	}

}
