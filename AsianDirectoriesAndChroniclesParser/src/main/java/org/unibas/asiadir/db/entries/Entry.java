package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface Entry extends Comparable<Entry> {
	
	// Initialises values
	public void fillEntry(ResultSet resultSet) throws SQLException;
	
	// 
	public String getSelectSQL();
	public String getSelectSQL(String whereClause);
	public String getByIdSQL(int id);
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException;
	
	
	public int getId();
	public String getName();
	
	// Override
	@Override
	public boolean equals(Object o);
	
	@Override
	public int hashCode();
	
	@Override
	public String toString();

	

}
