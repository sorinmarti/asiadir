package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TownEntry implements Entry {

	private int id;
	private String name;
	private int countryId;
	private int tgn;
	private String lat;
	private String lon;
	
	public TownEntry() {
		id = 0;
		name = "";
		countryId = 0;
		tgn = 0;
		lat = "0.0";
		lon = "0.0";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id   = resultSet.getInt(1);
		name = resultSet.getString(2);
		countryId = resultSet.getInt(3);
		tgn = resultSet.getInt(4);
		lat = resultSet.getString(5);
		lon = resultSet.getString(6);
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getLon() {
		return lon;
	}
	
	public int getCountryId() {
		return countryId;
	}
	
	public String toString() {
		return name;
	}

	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `town` (NAME, COUNTRY_ID, TGN, LAT, LON) values (?, ?, ?, ?, ?)");
			preparedStmt.setString(1, name);
			preparedStmt.setInt(2, countryId);
			preparedStmt.setInt(3, tgn);
			preparedStmt.setString(4, lat);
			preparedStmt.setString(5, lon);
			
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `town` SET NAME=?, COUNTRY_ID=?, TGN=?, LAT=?, LON=? where ID = ?");
			preparedStmt.setString(1, name);
			preparedStmt.setInt(2, countryId);
			preparedStmt.setInt(3, tgn);
			preparedStmt.setString(4, lat);
			preparedStmt.setString(5, lon);
			preparedStmt.setInt(6, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `town` ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `town` WHERE "+whereClause+" ORDER BY `NAME` ASC";
	}

	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `town` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof TownEntry) {
			if(((TownEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public int compareTo(Entry o) {
		return name.compareTo(o.getName());
	}
}
