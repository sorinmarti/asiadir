package org.unibas.asiadir.db.agencies;

public class Zyklus {

	private static int numberOfAdultCycles = 10;
	private static int numberOfChildCycles = 4;
	private static int[] adults   = new int[numberOfAdultCycles];
	private static int[] children = new int[numberOfChildCycles];
	private static int startingPop = 2;
	
	private static int minChildrenPerPairPerCycle = 0;
	private static int maxChildrenPerPairPerCycle = 4;
	
	private static long currentPop = 0;
	private static int currentCycle = 0;
	
	public static void main(String[] args) {
		adults[0] = startingPop;
		currentPop = calculatePop();
		
		while(currentCycle<100 && currentPop>0) {
			currentCycle++;
			currentPop = calculatePop();
			runRound();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private static void runRound() {
		printState();
		// Children grow:
		// All near adult children become adults
		adults[0] += children[children.length-1];
		// All other children grow
		int[] newChildren = new int[4];
		for(int i=0;i<(children.length-1);i++) {
			newChildren[i+1] = children[i];
		}
		children = newChildren;
		
		// Adults grow
		int newAdults[] = new int[numberOfAdultCycles];
		int bornChildren = 0;
		for(int i=0;i<(numberOfAdultCycles-1);i++) {
			newAdults[i+1] = adults[i];
			int childrenThisRound = (int) Math.floor(Math.random()*maxChildrenPerPairPerCycle);
			bornChildren += (Math.floor(adults[i]/2)*childrenThisRound);
		}
		children[0] = bornChildren;
		adults = newAdults;
		
	}
	
	private static int calculatePop() {
		int pop = 0;
		for(int adult : adults) {
			pop += adult;
		}
		for(int child : children) {
			pop += child;
		}
		return pop;
	}
	
	private static void printState() {
		System.out.println("Running Cycle "+currentCycle);
		System.out.println("  Current pop is "+currentPop);
		System.out.print  ("    (Adults:   |");
		for(int adult : adults) {
			System.out.print(String.format("%03d", adult)+"|");
		}
		System.out.println(")");
		System.out.print  ("    (Children: |");
		for(int child : children) {
			System.out.print(String.format("%03d", child)+"|");
		}
		System.out.println(")");
		System.out.println("----------------------------------");
	}
}
