package org.unibas.asiadir.db.agencies;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.TownEntry;

public class DataFormatter {

	public static void main(String[] args) {
		Set<TownEntry> towns = DatabaseService.getInstance().getTownEntries();
		Set<AgencyEntry> entries = DatabaseService.getInstance().getAgencyEntries();
		
		List<DataLine> lines = new ArrayList<>();
		
		for(TownEntry t : towns) {
			//System.out.println("Check "+t.getName());
			DataLine line = new DataLine(t.getName(), t.getLat(), t.getLon());
			for(AgencyEntry ae : entries) {
				if(ae.getIssue()==1920 && ae.getTown()==t.getId() && ae.getRepresents()==65) {
					line.addEntry(ae);
				}
			}
			if(line.hasEntries()) {
				lines.add(line);
			}
		}
		
		for(DataLine dl : lines) {
			System.out.println( dl.getLine() );
		}
	}
	
	static class DataLine {
		
		String town;
		String lat;
		String lon;
		List<AgencyEntry> entries = new ArrayList<>();
		
		public DataLine(String townName, String lat, String lon) {
			this.town = townName;
			this.lat = lat;
			this.lon = lon;
		}
		
		public void addEntry(AgencyEntry ae) {
			entries.add(ae);
		}
		
		public boolean hasEntries() {
			return entries.size()>0;
		}
		
		public String getLine() {
			return town+";"+lat+";"+lon+";"+entries.size();
		}
	}
}
