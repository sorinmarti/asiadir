package org.unibas.asiadir.db;

import java.sql.SQLException;
import java.util.List;

import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.Entry;
import org.unibas.asiadir.db.entries.NationEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;
import org.unibas.asiadir.db.entries.TownEntry;
import org.unibas.asiadir.gui.MainFrame;

public class MyTestClass {


	public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		/*
		List<Entry> entries = DatabaseService.getEntriesFromDB(AgencyEntry.class);
		showList(entries);
		
		entries = DatabaseService.getEntriesFromDB(CompanyEntry.class);
		showList(entries);
		
		entries = DatabaseService.getEntriesFromDB(NormalizedAgencyEntry.class);
		showList(entries);
		
		entries = DatabaseService.getEntriesFromDB(NormalizedCompanyEntry.class);
		showList(entries);
		
		entries = DatabaseService.getEntriesFromDB(NationEntry.class);
		showList(entries);
		
		entries = DatabaseService.getEntriesFromDB(TownEntry.class);
		showList(entries);
		*/
		
		/*
		Entry e = DatabaseService.getEntryById(AgencyEntry.class, 1);
		System.out.println("Name: "+e.getName());
		
		Entry e2 = DatabaseService.getEntryById(CompanyEntry.class, 49);
		System.out.println("Name: "+e2.getName());
		
		Entry e3 = DatabaseService.getEntryById(NormalizedAgencyEntry.class, 8);
		System.out.println("Name: "+e3.getName());
		
		Entry e4 = DatabaseService.getEntryById(NormalizedCompanyEntry.class, 8);
		System.out.println("Name: "+e4.getName());
		
		Entry e5 = DatabaseService.getEntryById(NationEntry.class, 8);
		System.out.println("Name: "+e5.getName());
		
		Entry e6 = DatabaseService.getEntryById(TownEntry.class, 8);
		System.out.println("Name: "+e6.getName());
		*/
		
		MainFrame frame = new MainFrame();
		frame.setVisible(true);
	}
	
	private static void showList(List<Entry> entries)  {
		for(Entry e : entries) {
			System.out.println(e.getId() + ": "+ e.getName());
		}
	}
	
}
