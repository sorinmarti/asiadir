package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NationEntry implements Entry {

	private int id;
	private String name;
	
	public NationEntry() {
		id = 0;
		name = "";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id   = resultSet.getInt(1);
		name = resultSet.getString(2);
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}

	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `country` (NAME) values (?)");
			preparedStmt.setString(1, name);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `country` SET NAME=? where ID = ?");
			preparedStmt.setString(1, name);
			preparedStmt.setInt(2, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `country` ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `country` WHERE "+whereClause+" ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `country` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof NationEntry) {
			if(((NationEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public int compareTo(Entry o) {
		return name.compareTo(o.getName());
	}
}
