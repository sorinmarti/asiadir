package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NormalizedAgencyEntry implements Entry {

	private int id;
	private int type;
	private String name;
	private String notes;
	private int nationality;
	private int classType;
	private String classSource;
	
	public NormalizedAgencyEntry() {
		id          = 0;
		type        = 0;
		name        = "";
		notes = "";
		nationality  = 6;
		classType = 0;
		classSource = "";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id              = resultSet.getInt(1);
		type            = resultSet.getInt(2);
		name            = resultSet.getString(3);
		notes           = resultSet.getString(4);
		nationality     = resultSet.getInt(5);
		classType       = resultSet.getInt(6);
		classSource     = resultSet.getString(7);
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNotes() {
		return notes;
	}
	
	public void setName(String nName) {
		name = nName;
	}
	
	public String toString() {
		return name;
	}

	public int getNationality() {
		return nationality;
	}
	
	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `agency` (TYPE, NAME, NOTES, NATIONALITY, CLASSIFICATION_TYPE, CLASSIFICATION_SOURCE) values (?, ?, ?, ?, ?, ?)");
			preparedStmt.setInt(1, type);
			preparedStmt.setString(2, name);
			preparedStmt.setString(3, notes);
			preparedStmt.setInt(4, nationality);
			preparedStmt.setInt(5, classType);
			preparedStmt.setString(6, classSource);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `agency` SET TYPE=?, NAME=?, NOTES=?, NATIONALITY=?, CLASSIFICATION_TYPE=?, CLASSIFICATION_SOURCE=? where ID = ?");
			preparedStmt.setInt(1, type);
			preparedStmt.setString(2, name);
			preparedStmt.setString(3, notes);
			preparedStmt.setInt(4, nationality);
			preparedStmt.setInt(5, classType);
			preparedStmt.setString(6, classSource);
			preparedStmt.setInt(7, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `agency` ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `agency` WHERE "+whereClause+" ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `agency` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof NormalizedAgencyEntry) {
			if(((NormalizedAgencyEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public int compareTo(Entry o) {
		return name.compareTo(o.getName());
	}
}
