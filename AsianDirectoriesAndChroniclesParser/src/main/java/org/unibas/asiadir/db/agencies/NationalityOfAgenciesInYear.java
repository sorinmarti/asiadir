package org.unibas.asiadir.db.agencies;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.NationEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.TownEntry;

public class NationalityOfAgenciesInYear {

	public static void main(String args[]) {
		Set<NormalizedAgencyEntry> nAgencies = DatabaseService.getInstance().getNAgencyEntries();
		Map<Integer, NationalityHelper> nations = new HashMap<>();
		
		// For all normalized agencies...
		for(NormalizedAgencyEntry aEntry : nAgencies) {
			//...which are present in the year:
			if(agencyIsInYear(aEntry, 1920)) {
				// Ask for the nationality.
				Integer nationality = new Integer(aEntry.getNationality());
				// If it is not yet present in the list: create it
				if(!nations.containsKey(nationality)) {
					NationalityHelper helper = new NationalityHelper(nationality);
					nations.put(nationality, helper);
				}
				// Else increase the number of occurrences 
				else {
					nations.get(nationality).increaseAgencies();
				}
			}
		}
		
		// Print the results
		for (Map.Entry<Integer, NationalityHelper> entry : nations.entrySet()) {
			NationEntry natEntry = DatabaseService.getInstance().getNationById(entry.getKey().intValue());
			System.out.println(natEntry.getName()+";"+entry.getValue().numberOfAgencies);
		}
	}

	private static boolean agencyIsInYear(NormalizedAgencyEntry entry, int year) {
		Set<AgencyEntry> aEntries =  DatabaseService.getInstance().getAgencyEntries();
		for(AgencyEntry e : aEntries) {
			if(e.getRepresents()==entry.getId() && (e.getIssue()==year || year==0)) {
				return true;
			}
		}
		return false;
	}
	
	private static class NationalityHelper {
		private final int nationality;
		private int numberOfAgencies = 0;
		
		NationalityHelper(int nationality) {
			this.nationality = nationality;
			numberOfAgencies = 1;
		}
		
		protected void increaseAgencies() {
			numberOfAgencies++;
		}
		
		
	}
}
