package org.unibas.asiadir.db.agencies;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;

import net.miginfocom.swing.MigLayout;

public class AgencyGroupingForm extends JFrame {

	private JPanel contentPane;
	private JTextField txtNewGroup;
	private JList<AgencyEntry> agencyEntriesList;
	private JList<NormalizedAgencyEntry> normalizedAgencyList;
	private DefaultListModel<NormalizedAgencyEntry> listModel;
	
	private int nextEntry;
	private List<AgencyEntry> listEntries;
	
	
	/**
	 * Create the frame.
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public AgencyGroupingForm() throws ClassNotFoundException, SQLException {		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 707, 396);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][grow]", "[][fill][][][][grow][][][]"));
		
		JLabel lblUngroupedAgencies = new JLabel("Ungrouped Agencies");
		contentPane.add(lblUngroupedAgencies, "cell 0 0");
		
		JLabel lblGrouping = new JLabel("Grouping");
		contentPane.add(lblGrouping, "cell 1 0");
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 1 1 8,grow");
		
		agencyEntriesList = new JList<>();
		scrollPane.setViewportView(agencyEntriesList);
		
		JLabel lblCreateNewGroup = new JLabel("Create New Group:");
		contentPane.add(lblCreateNewGroup, "cell 1 1");
		
		txtNewGroup = new JTextField();
		contentPane.add(txtNewGroup, "cell 1 2,growx");
		txtNewGroup.setColumns(10);
		
		JButton btnAddGroup = new JButton("Create Group");
		btnAddGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				createGroup(txtNewGroup.getText());
			}
		});
		contentPane.add(btnAddGroup, "flowx,cell 1 3");
		
		JLabel lblAddToExisting = new JLabel("Add to existing Group:");
		contentPane.add(lblAddToExisting, "cell 1 4");
		
		listModel = new DefaultListModel<NormalizedAgencyEntry>();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "cell 1 5 1 3,grow");
		normalizedAgencyList = new JList(listModel);
		scrollPane_1.setViewportView(normalizedAgencyList);
		
		JButton btnGetText = new JButton("Get Text");
		btnGetText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNewGroup.setText(agencyEntriesList.getSelectedValue().getName());
			}
		});
		contentPane.add(btnGetText, "cell 1 3");
		
		JLabel lblStats = new JLabel("");
		contentPane.add(lblStats, "flowx,cell 1 8");
		
		JButton btnAddToGroup = new JButton("Add to Group");
		btnAddToGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addToGroup();
			}
		});
		contentPane.add(btnAddToGroup, "cell 1 8");
		
		JLabel lblReloadForm = new JLabel("Reload Form:");
		contentPane.add(lblReloadForm, "cell 1 8");
		
		JButton btnReload = new JButton("Reload");
		btnReload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					updateForm();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		contentPane.add(btnReload, "cell 1 8");
		
		JButton btnGetCreate = new JButton("Get / Create / Add");
		btnGetCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Get the text from the agencies records
				int selectedIndex = agencyEntriesList.getSelectedIndex();
				String text = agencyEntriesList.getSelectedValue().getName();
				// Create a group with this name
				createGroup(text);
				
				agencyEntriesList.setSelectedIndex(selectedIndex);
				// Add the agency record to the group
				addToGroup();
			}
		});
		contentPane.add(btnGetCreate, "cell 1 3");
		
		updateForm();
	}

	private void getAgencyEntries() throws ClassNotFoundException, SQLException {
		Set<AgencyEntry> entries = DatabaseService.getInstance().getAgencyEntries();
		List<AgencyEntry> filteredEntries = new ArrayList<>();
		for(AgencyEntry ae : entries) {
			if(ae.getRepresents()==1) {
				filteredEntries.add(ae);
			}
		}
		Collections.sort(filteredEntries);
		agencyEntriesList.setListData(filteredEntries.toArray(new AgencyEntry[filteredEntries.size()]));
	}
	
	/*
	public static <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
	  List<T> list = new ArrayList<T>(c);
	  Collections.sort(list);
	  return list;
	}
	*/
	
	private void getNormalizedAgencyEntries() throws ClassNotFoundException, SQLException {
		System.out.println("Old size: "+listModel.getSize());
		Set<NormalizedAgencyEntry> entries = DatabaseService.getInstance().getNAgencyEntries();
		List<NormalizedAgencyEntry> sortedEntries = new ArrayList<>();
		for(NormalizedAgencyEntry e : entries) {
			sortedEntries.add(e);
		}
		Collections.sort(sortedEntries);
		
		listModel.clear();
		
		for(NormalizedAgencyEntry e2 : sortedEntries) {
			listModel.addElement(e2);
		}
		normalizedAgencyList.updateUI();
		System.out.println("New size: "+listModel.getSize());
	}
	
	private void updateForm() throws ClassNotFoundException, SQLException {
		getAgencyEntries();
		getNormalizedAgencyEntries();
	}

	private void createGroup(String text) {
		try {
			MainClassAgencies.createNormalizedAgencyRecord(text);
			updateForm();
			for(int i=0;i<listModel.getSize();i++) {
				NormalizedAgencyEntry e = listModel.elementAt(i);
				if(e.getName().equals(text)) {
					normalizedAgencyList.setSelectedIndex(i);
					normalizedAgencyList.ensureIndexIsVisible(i);
					
					break;
				}
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void addToGroup() {
		List<AgencyEntry> entries = agencyEntriesList.getSelectedValuesList();
		NormalizedAgencyEntry ag = (NormalizedAgencyEntry) listModel.getElementAt(normalizedAgencyList.getSelectedIndex());
		
		try {
			MainClassAgencies.updateAgencyEntries(ag.getId(), entries);
			updateForm();
		} catch (ClassNotFoundException | SQLException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		}
	}
}
