package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NormalizedCompanyEntry implements Entry {

	private int id;
	private int type;
	private String name;
	private int nationality;
	private int classType;
	private String classSource;
	
	public NormalizedCompanyEntry() {
		id             = 0;
		type = 0;
		name  = "";
		nationality     = 0;
		classType = 0;
		classSource = "";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id              = resultSet.getInt(1);
		type            = resultSet.getInt(2);
		name            = resultSet.getString(3);
		nationality     = resultSet.getInt(4);
		classType       = resultSet.getInt(5);
		classSource     = resultSet.getString(6);
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}
	
	public int getNationality() {
		return nationality;
	}

	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `company` (TYPE, NAME, NATIONALITY, CLASSIFICATION_TYPE, CLASSIFICATION_SOURCE) values (?, ?, ?, ?, ?)");
			preparedStmt.setInt(1, type);
			preparedStmt.setString(2, name);
			preparedStmt.setInt(3, nationality);
			preparedStmt.setInt(4, classType);
			preparedStmt.setString(5, classSource);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `company` SET TYPE=?, NAME=?, NATIONALITY=?, CLASSIFICATION_TYPE=?, CLASSIFICATION_SOURCE=? where ID = ?");
			preparedStmt.setInt(1, type);
			preparedStmt.setString(2, name);
			preparedStmt.setInt(3, nationality);
			preparedStmt.setInt(4, classType);
			preparedStmt.setString(5, classSource);
			preparedStmt.setInt(6, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `company` ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `company` WHERE "+whereClause+" ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `company` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof NormalizedCompanyEntry) {
			if(((NormalizedCompanyEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public int compareTo(Entry o) {
		return name.compareTo(o.getName());
	}
}
