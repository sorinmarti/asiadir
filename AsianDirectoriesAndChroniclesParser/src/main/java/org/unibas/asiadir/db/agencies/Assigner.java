package org.unibas.asiadir.db.agencies;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;

import info.debatty.java.stringsimilarity.NormalizedLevenshtein;

public class Assigner {

	public static void main(String[] args) throws IOException {
		Set<AgencyEntry> entrySet =  DatabaseService.getInstance().getAgencyEntries();
		Set<NormalizedAgencyEntry> nEntrySet =  DatabaseService.getInstance().getNAgencyEntries();
		
		List<AgencyEntry> entries = new ArrayList<AgencyEntry>();
		Map<Integer, NormalizedAgencyEntry> nEntries = new HashMap<Integer, NormalizedAgencyEntry>();
		
		for(AgencyEntry e : entrySet) {
			if(e.getRepresents()==1) {
				entries.add(e);
			}
		}
		System.out.println("Found "+entrySet.size()+" agency entries, "+entries.size()+" are unassigned.");
		Collections.sort(entries);
		
		for(NormalizedAgencyEntry ne : nEntrySet) {
			nEntries.put(ne.getId(), ne);
		}
		System.out.println("Found "+nEntrySet.size()+" agencies, "+nEntries.size()+" are used.");
		
		//Levenshtein l = new Levenshtein();
		NormalizedLevenshtein lev = new NormalizedLevenshtein();
		for(AgencyEntry e : entries) {
			String agencyString = e.getName();
			int smallestDistanceId = 0;
			double smallestDistance = Double.MAX_VALUE;
			Iterator<Map.Entry<Integer, NormalizedAgencyEntry>> it = nEntries.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<Integer, NormalizedAgencyEntry> pair = (Map.Entry)it.next();
		        //System.out.println(pair.getKey() + " = " + pair.getValue());
		        double distance = lev.distance(agencyString, pair.getValue().getName());
				if(distance<smallestDistance) {
					smallestDistance = distance;
					smallestDistanceId = pair.getKey();
				}
		        //it.remove(); // avoids a ConcurrentModificationException
		    }
		    
		    if(smallestDistance<.25) {
				System.out.println("Smallest distance ("+smallestDistance+") for "+smallestDistanceId+":");
				System.out.println("    "+agencyString);
				System.out.println("    "+nEntries.get(smallestDistanceId));
				System.out.println("Is this correct? Y/N?");
				 //Enter data using BufferReader 
		        BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in)); 
		        String answer = reader.readLine(); 
		        if(answer.toLowerCase().equals("y")) {
		        	System.out.println("----> UPDATED AGENCY Record "+e.getId());
		        	e.setRepresents(smallestDistanceId);
		        	DatabaseService.getInstance().updateAgencyEntry(e);
		        	//AgencyEntry aE = nEntries.get(smallestDistanceId);
		        }
		        else {
		        	System.out.println("--> IGNORE AGENCY Record");
		        }
		    }
		    else {
		    	System.out.println("--> Ignored entry (distance too big) "+agencyString);
		    }
			
		}
	}

}
