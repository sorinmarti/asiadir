package org.unibas.asiadir.db.agencies;

import java.io.IOException;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;

public class AgencyRecordUpdater {

	public static void main(String[] args) throws IOException {
		Set<AgencyEntry> aEntrySet =  DatabaseService.getInstance().getAgencyEntries();
		
		for(AgencyEntry ae : aEntrySet) {
			//String test = ae.getId()+" / "+ae.getIssue()+" / "+ae.getTown()+" / "+ae.getCompanyRecord()+" / "+ae.getRepresents()+" / "+ae.getName();
			CompanyEntry ce = DatabaseService.getInstance().getCompanyEntry(ae.getCompanyRecord());
			//if(ae.getId()==1) {
				System.out.println(ce.getId()+" / "+ce.getIssue()+" / "+ce.getTown());
				ae.setIssue( ce.getIssue() );
				ae.setTown( ce.getTown() );
				DatabaseService.getInstance().updateAgencyEntry(ae);
			//}
		}
	}

}
