package org.unibas.asiadir.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.Entry;
import org.unibas.asiadir.db.entries.IssueEntry;
import org.unibas.asiadir.db.entries.NationEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;
import org.unibas.asiadir.db.entries.TownEntry;

public class DatabaseService {

	private static DatabaseService instance = null;
	
	private Set<CompanyEntry> companies = new HashSet<CompanyEntry>();
	private Set<NormalizedCompanyEntry> nCompanies = new HashSet<NormalizedCompanyEntry>();
	private Set<AgencyEntry> agencies = new HashSet<AgencyEntry>();
	private Set<NormalizedAgencyEntry> nAgencies = new HashSet<NormalizedAgencyEntry>();
	private Set<NationEntry> nations = new HashSet<NationEntry>();
	private Set<TownEntry> towns = new HashSet<TownEntry>();
	private Set<IssueEntry> issues = new HashSet<IssueEntry>();
	
	public static DatabaseService getInstance() {
		if(instance==null) {
			instance = new DatabaseService();
		}
		return instance;
	}
	
	private DatabaseService() {
		updateFromDatabase();
	}
	
	//////////
	// COMPANIES
	public Set<CompanyEntry> getCompanyEntries() {
		return companies;
	}
	
	public Set<CompanyEntry> getCompanyEntriesOfCompany(int id) {
		Set<CompanyEntry> companyEntries = new HashSet<CompanyEntry>();
		for(CompanyEntry e : companies) {
			if(e.getRepresents()==id) {
				companyEntries.add(e);
			}
		}
		return companyEntries;
	}
	
	public CompanyEntry getCompanyEntry(int id) {
		for(CompanyEntry e : companies) {
			if(e.getId()==id) {
				return e;
			}
		}
		return null;
	}
	
	public void updateCompanyEntry(CompanyEntry e) {
		updateEntry(e);
	}
	
	//////////
	// AGENCIES
	public Set<AgencyEntry> getAgencyRecordsOfCompanyRecord(int id) {
		Set<AgencyEntry> agencyEntries = new HashSet<>();
		for(AgencyEntry entry : agencies) {
			if(entry.getCompanyRecord()==id) {
				agencyEntries.add(entry);
			}
		}
		return agencyEntries;
	}
	
	public Set<AgencyEntry> getAgencyRecordsOfNAgency(int nAgencyId) {
		Set<AgencyEntry> agencyEntries = new HashSet<>();
		for(AgencyEntry entry : agencies) {
			if(entry.getRepresents()==nAgencyId) {
				agencyEntries.add(entry);
			}
		}
		return agencyEntries;
	}
	
	public Set<AgencyEntry> getAgencyEntries() {
		return agencies;
	}
	
	public void createAgencyEntry(AgencyEntry e) {
		createEntry(e);
	}
	
	public void updateAgencyEntry(AgencyEntry e) {
		updateEntry(e);
	}
	
	//////////
	// N-AGENCIES
	public Set<NormalizedCompanyEntry> getNCompanyEntries() {
		return nCompanies;
	}
	
	public NormalizedCompanyEntry getNCompanyById(int id) {
		for(NormalizedCompanyEntry e : nCompanies) {
			if(id==e.getId()) {
				return e;
			}
		}
		return null;
	}
	
	public Set<NormalizedAgencyEntry> getNAgencyEntries() {
		return nAgencies;
	}
	
	public NormalizedAgencyEntry getNAgencyById(int id) {
		for(NormalizedAgencyEntry e : nAgencies) {
			if(id==e.getId()) {
				return e;
			}
		}
		return null;
	}
	
	public void createNormalizedAgencyEntry(NormalizedAgencyEntry e) {
		createEntry(e);
	}
	
	public NormalizedAgencyEntry getAgenciesOfAgencyRecord(int represents) {
		for(NormalizedAgencyEntry agency : nAgencies) {
			if(agency.getId()==represents) {
				return agency;
			}
		}
		return null;
	}	
	
	public Set<NationEntry> getNationEntries() {
		return nations;
	}
	
	public NationEntry getNationById(int id) {
		for(NationEntry e : nations) {
			if(id==e.getId()) {
				return e;
			}
		}
		return null;
	}
	
	public Set<TownEntry> getTownEntries() {
		return towns;
	}
	
	public TownEntry getTownById(int id) {
		for(TownEntry e : towns) {
			if(id==e.getId()) {
				return e;
			}
		}
		return null;
	}
	
	public Set<IssueEntry> getIssueEntries() {
		return issues;
	}
	
	@SuppressWarnings("unchecked")
	private void updateFromDatabase() {
		try {
			companies  = (Set<CompanyEntry>) getEntriesFromDB(CompanyEntry.class);
			nCompanies = (Set<NormalizedCompanyEntry>) getEntriesFromDB(NormalizedCompanyEntry.class);
			agencies   = (Set<AgencyEntry>) getEntriesFromDB(AgencyEntry.class);
			nAgencies  = (Set<NormalizedAgencyEntry>) getEntriesFromDB(NormalizedAgencyEntry.class);
			nations    = (Set<NationEntry>) getEntriesFromDB(NationEntry.class);
			towns      = (Set<TownEntry>) getEntriesFromDB(TownEntry.class);
			issues     = (Set<IssueEntry>) getEntriesFromDB(IssueEntry.class);
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		String connectionUrl = "jdbc:mysql://localhost:3306/asiadir?useUnicode=true&characterEncoding=UTF-8&user=root&password=";
		Connection conn = DriverManager.getConnection(connectionUrl);
		return conn;
	}
	
	private static Set<? extends Entry> getEntriesFromDB(Class<? extends Entry> listClass) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		//List<Entry> entries = new ArrayList<>();
		Set<Entry> entries = new HashSet<>();
		
		String sql = null;
		sql = listClass.newInstance().getSelectSQL();
		
		if(sql!=null) {
			Connection conn = getConnection(); 
			ResultSet rs = conn.prepareStatement(sql).executeQuery();
			 while(rs.next()){
				 Entry createdObject = listClass.newInstance();
				 createdObject.fillEntry(rs);
				 entries.add(createdObject);
			 }
			 conn.close();
		}
		return entries;
	}

	private void updateEntry(Entry entry) {
		try {
			Connection conn = getConnection(); 
			PreparedStatement statement = entry.getSaveSQL(conn);
			statement.executeUpdate();  
			conn.close();
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		updateFromDatabase();
	}
	
	private void createEntry(Entry entry) {
		try {
			Connection conn = getConnection(); 
			PreparedStatement statement = entry.getSaveSQL(conn);
			statement.execute();  
			conn.close();
		} catch(ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		updateFromDatabase();
	}
	
	private static Entry getEntryByIdFromDB(Class<? extends Entry> entryClass, int id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Entry entry = entryClass.newInstance();
		
		String sql = entry.getByIdSQL(id);
		
		if(sql!=null) {
			Connection conn = getConnection();
			ResultSet rs = conn.prepareStatement(sql).executeQuery();
			rs.next();
			entry.fillEntry(rs);
			conn.close();
			return entry;
		}
		return null;
	}
	
}
