package org.unibas.asiadir.db.agencies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;

public class DataFormatter2 {

	public static void main(String[] args) throws FileNotFoundException, IOException { 
		createNodes(1900);
		createNodes(1920);
		createEdges(1900);
		createEdges(1920);
	}
	
	private static void createEdges(int year) throws FileNotFoundException, IOException {
		Set<AgencyEntry> aEntries = DatabaseService.getInstance().getAgencyEntries();
		
		int i = 0;
		String out = "Source;Target;Type;Id;Label;timeset;Weight\n";
		for(AgencyEntry e : aEntries) {
			// For an agency entry: get its normalized agency entry and its normalized company entry
			CompanyEntry ce = DatabaseService.getInstance().getCompanyEntry(e.getCompanyRecord());
			NormalizedCompanyEntry nce = DatabaseService.getInstance().getNCompanyById(ce.getRepresents());
			NormalizedAgencyEntry nae = DatabaseService.getInstance().getNAgencyById(e.getRepresents());
			
			if(ce.getIssue()==year) {
				if(nae.getId()!=1) {	// not UNDEFINED
					out += nce.getId()+";"+(nae.getId()+10000)+";Undirected;"+i+";;;1\n";
					i++;
				}
			}
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(year+"_edges.csv")), StandardCharsets.UTF_8)) {
			 writer.write(out);
			 writer.close();
		}
	}
	
	private static void createNodes(int year) throws FileNotFoundException, IOException {
		List<NormalizedCompanyEntry> cEntriesInYear = getNCompaniesOfYear(year);
		List<NormalizedAgencyEntry> aEntriesInYear = getNAgenciesOfYear(year);
		String out = "id;label;category\n";
		
		for(NormalizedCompanyEntry nce : cEntriesInYear) {
			out += nce.getId()+";"+nce.getName()+";Local\n";
		}
		for(NormalizedAgencyEntry nce : aEntriesInYear) {
			out += (nce.getId()+10000)+";"+nce.getName()+";Represented\n";
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(year+"_nodes.csv")), StandardCharsets.UTF_8)) {
			 writer.write(out);
			 writer.close();
		}
	}
	
	/**
	 * 
	 * @param year
	 * @return
	 */
	private static List<NormalizedCompanyEntry> getNCompaniesOfYear(int year) {
		// GET N-COMPANIES FOR THE YEARS
		Set<CompanyEntry> entries = DatabaseService.getInstance().getCompanyEntries();
		List<NormalizedCompanyEntry> entriesInYear = new ArrayList<>();
		for(CompanyEntry e : entries) {
			if(e.getIssue()==year) {	// not UNDEFINED
				NormalizedCompanyEntry nce = DatabaseService.getInstance().getNCompanyById(e.getRepresents());
				if(!entriesInYear.contains(nce)) {
					entriesInYear.add(nce);
				}
			}
		}
		return entriesInYear;		
	}
	
	/**
	 * 
	 * @param year
	 * @return
	 */
	private static List<NormalizedAgencyEntry> getNAgenciesOfYear(int year) {
		Set<AgencyEntry> aentries = DatabaseService.getInstance().getAgencyEntries();
		List<NormalizedAgencyEntry> aentriesInYear = new ArrayList<>();
		for(AgencyEntry e : aentries) {
			if(e.getRepresents()!=1 && e.getIssue()==year) {
				NormalizedAgencyEntry nce = DatabaseService.getInstance().getNAgencyById(e.getRepresents());
				if(!aentriesInYear.contains(nce)) {
					aentriesInYear.add(nce);
				}
			}
		}
		return aentriesInYear;
	}
}
