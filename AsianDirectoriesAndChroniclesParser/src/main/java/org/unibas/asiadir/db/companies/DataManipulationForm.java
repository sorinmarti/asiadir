package org.unibas.asiadir.db.companies;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.CompanyEntry;

import net.miginfocom.swing.MigLayout;

public class DataManipulationForm extends JFrame {

	private JPanel contentPane;
	private JTextArea textArea;
	private JList<String> list;
	
	private int nextEntry;
	private List<CompanyEntry> entries;
	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	
	/**
	 * Create the frame.
	 * @param entries 
	 */
	public DataManipulationForm() {
		this(new ArrayList<CompanyEntry>());
	}
	
	public DataManipulationForm(List<CompanyEntry> entries) {
		this.nextEntry = 0;
		this.entries = entries;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 662, 599);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[289.00,grow][grow]", "[][grow][]"));
		
		JLabel lblOriginalTextFrom = new JLabel("Original Text from Directories");
		panel.add(lblOriginalTextFrom, "cell 0 0");
		
		JLabel lblStructuredData = new JLabel("Structured Data");
		panel.add(lblStructuredData, "cell 1 0");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane_1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel.add(scrollPane_1, "cell 0 1,grow");
		
		textArea = new JTextArea();
		scrollPane_1.setViewportView(textArea);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel.add(scrollPane, "cell 1 1,grow");
		
		list = new JList<>();
		list.setModel(listModel);
		scrollPane.setViewportView(list);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1, "cell 0 2 2 1,grow");
		
		JButton btnThisIsCorrect = new JButton("This is correct -> SAVE");
		btnThisIsCorrect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//int id = DataManipulationForm.this.entries.get(nextEntry-1).getId();
				
				// SAVE DATA: Create new company entries
				try {
					List<String> agencies = new ArrayList<>();
					for(int i = 0; i< list.getModel().getSize();i++) {
			            agencies.add(list.getModel().getElementAt(i));
			        }
					//MainClassCompanies.createAgencyRecords(id, agencies);
					MainClassCompanies.createAgencyRecords(DataManipulationForm.this.entries.get(nextEntry-1), agencies);
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(DataManipulationForm.this, "Could not insert agency entry");
				}
				
				// SET STATE
				try {
					//MainClassCompanies.updateCompanyState(id);
					MainClassCompanies.updateCompanyState(DataManipulationForm.this.entries.get(nextEntry-1));
				} catch (ClassNotFoundException | SQLException e) {
					JOptionPane.showMessageDialog(DataManipulationForm.this, "Could not update company entry");
				}
				
				// SHOW NEXT
				showNextEntry();
			}
		});
		
		JButton btnUpdateEntry = new JButton("Update Entry and Reparse");
		btnUpdateEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CompanyEntry entry = entries.get(nextEntry-1);
				entry.setName(textArea.getText());
				DatabaseService.getInstance().updateCompanyEntry(entry);
				reparseAgencyList(entry);
			}
		});
		panel_1.add(btnUpdateEntry);
		
		JButton buttonPlus = new JButton("+");
		buttonPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = JOptionPane.showInputDialog(DataManipulationForm.this, "Agency name", textArea.getSelectedText());
				if(text!=null) {
					listModel.addElement(text);
				}
			}
		});
		panel_1.add(buttonPlus);
		
		JButton buttonMinus = new JButton("-");
		buttonMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel.remove(list.getSelectedIndex());
			}
		});
		panel_1.add(buttonMinus);
		panel_1.add(btnThisIsCorrect);
		
		JButton btnThisHasErrors = new JButton("This has errors -> IGNORE");
		btnThisHasErrors.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// SHOW NEXT
				showNextEntry();
			}
		});
		panel_1.add(btnThisHasErrors);
	}
	
	public void showNextEntry() {
		if(entries.size()==nextEntry) {
			JOptionPane.showMessageDialog(DataManipulationForm.this, "Finished list.");
			System.exit(0);
		}
		
		CompanyEntry entry = entries.get(nextEntry);
		System.out.println("show entry "+entry.getId());
		
		textArea.setText( entry.getName() );
		
		reparseAgencyList(entry);
		
		nextEntry++;
	}
	
	private void reparseAgencyList(CompanyEntry entry) {
		System.out.println("reparse Agency List "+entry.getId());
		String[] agencyLines = entry.getEntryLines();				
		listModel.removeAllElements();
		for(String line : agencyLines) {
			listModel.addElement(line);
		}
		//list.setListData(agencyLines);
	}

}
