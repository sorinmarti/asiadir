package org.unibas.asiadir.db.agencies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;

public class DataFormatter3 {

	public static void main(String[] args) throws FileNotFoundException, IOException { 
		createNodesAndEdgesForCompany(1900, 1);
		
	}
	
	private static void createNodesAndEdgesForCompany(int year, int company) throws FileNotFoundException, IOException {
		// Get company in question
		NormalizedCompanyEntry cEntry = DatabaseService.getInstance().getNCompanyById(company);
		String outNodes = "id;label;category\n";
		outNodes += cEntry.getId()+";"+cEntry.getName()+";Local\n";
		
		String outEdges = "Source;Target;Type;Id;Label;timeset;Weight\n"; 
		int edgeId = 0;
		
		// Get all agencies connected to the company in a year
		Set<AgencyEntry> aentries = DatabaseService.getInstance().getAgencyEntries();
		List<NormalizedAgencyEntry> aentriesInYear = new ArrayList<>();
		// Go through all agencies and sort out un-normalized and thos in other years
		for(AgencyEntry e : aentries) {
			if(e.getRepresents()!=1 &&		// Agency entry is normalized 
			   e.getIssue()==year) {		// Year is correct
				// Get the connected company entry
				CompanyEntry ce = DatabaseService.getInstance().getCompanyEntry(e.getCompanyRecord());
				if(ce.getRepresents()==company) {
					// The company entry is of the searched company: get the normalized agency of the original agency entry
					NormalizedAgencyEntry nae = DatabaseService.getInstance().getNAgencyById(e.getRepresents());
					if(!aentriesInYear.contains(nae)) {
						aentriesInYear.add(nae);
					}
				}
			}
		}
		
		for(NormalizedAgencyEntry nce : aentriesInYear) {	
			// Print the nAgency
			outNodes += (nce.getId()+10000)+";"+nce.getName()+";Represented\n";
			// All these NAgencies are connected to the NCompany
			outEdges += cEntry.getId()+";"+(nce.getId()+10000)+";Undirected;"+edgeId+";;;1\n";
			edgeId++;
		}
		
		// Now we have the NCompany and its connected NAgencies.
		// Now search for all companies connected with these NAgencies
		List<NormalizedCompanyEntry> additionalCompanyEntries = new ArrayList<>();
		for(AgencyEntry ae : aentries) {
			// If the entry is in the year: 
			if(ae.getIssue()==year) {
				boolean isInRepList = false;
				for(NormalizedAgencyEntry nce : aentriesInYear) {
					if(ae.getRepresents()==nce.getId()) {
						isInRepList = true;
						break;
					}
				}
				if(isInRepList) {
					//System.out.println(ae.getName()+" is in rep list.");
					CompanyEntry ce = DatabaseService.getInstance().getCompanyEntry(ae.getCompanyRecord());
					if(ce.getRepresents()!=company) {
						NormalizedCompanyEntry newNce = DatabaseService.getInstance().getNCompanyById(ce.getRepresents());
						if(!additionalCompanyEntries.contains(newNce)) {
							//System.out.println("--> therefore its representer '"+newNce.getName()+"' is added.");
							additionalCompanyEntries.add(newNce);
							// This NCompany (newNce) is connected to what ae represents:
							NormalizedAgencyEntry newAe = DatabaseService.getInstance().getNAgencyById(ae.getRepresents());
							outEdges += newNce.getId()+";"+(newAe.getId()+10000)+";Undirected;"+edgeId+";;;1\n";
							edgeId++;
						}
					}
				}
				
			}
		}
		
		for(NormalizedCompanyEntry nce : additionalCompanyEntries) {	
			// Print the nAgency
			outNodes += nce.getId()+";"+nce.getName()+";Local\n";
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(year+"_c"+company+"_nodes.csv")), StandardCharsets.UTF_8)) {
			 writer.write(outNodes);
			 writer.close();
		}
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File(year+"_c"+company+"_edges.csv")), StandardCharsets.UTF_8)) {
			 writer.write(outEdges);
			 writer.close();
		}
		// NOW for the edges:
		// cEntry is the origin NCompany
		// aentriesInYear are the connected NAgencies
		// additionalCompanyEntries are the connected NCompanies
		
	}
	
	

}
