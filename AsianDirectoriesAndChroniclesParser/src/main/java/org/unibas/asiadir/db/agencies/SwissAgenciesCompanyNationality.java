package org.unibas.asiadir.db.agencies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;
import org.unibas.asiadir.db.entries.NationEntry;
import org.unibas.asiadir.db.entries.NormalizedAgencyEntry;
import org.unibas.asiadir.db.entries.NormalizedCompanyEntry;
import org.unibas.asiadir.db.entries.TownEntry;

public class SwissAgenciesCompanyNationality {

	public static void main(String[] args) {
		// Swiss
		printCSVOfNationsInPlacesForYear(2, 1900);
		//printCSVOfNationsInPlacesForYear(1, 1920);
		
	}
	
	private static void printCSVOfNationsInPlacesForYear(int agencyNation, int year) {
		Set<NormalizedAgencyEntry> nAgencies = DatabaseService.getInstance().getNAgencyEntries();
		Set<AgencyEntry> agencies = DatabaseService.getInstance().getAgencyEntries();
		Set<CompanyEntry> companies = DatabaseService.getInstance().getCompanyEntries();
		List<NormalizedAgencyEntry> swissNAgencies = new ArrayList<>();
		
		for(NormalizedAgencyEntry entry : nAgencies) {
			if(entry.getNationality()==agencyNation) {
				swissNAgencies.add(entry);
			}
		}
		
		
		List<AgencyEntry> agencyRecordsOfAllSwissAgencies = new ArrayList<>();
		for(NormalizedAgencyEntry entry : swissNAgencies) {
			//System.out.println("Agency: "+entry.getName());
			for(AgencyEntry aEntry : agencies) {
				if(aEntry.getRepresents()==entry.getId() && aEntry.getIssue()==year) {
					//System.out.println("  AgencyRecord: "+aEntry.getName());
					agencyRecordsOfAllSwissAgencies.add(aEntry);
				}
			}
		}
		
		List<CompanyEntry> companyEntriesOfAllSwissAgencies = new ArrayList<>();
		List<TownEntry> mentionedTowns = new ArrayList<>();
		for(AgencyEntry aEntry : agencyRecordsOfAllSwissAgencies) {
			//System.out.println("AgencyRecord: "+aEntry.getName());
			CompanyEntry cEntry = DatabaseService.getInstance().getCompanyEntry(aEntry.getCompanyRecord());
			//System.out.println("  CompanyRecord: "+cEntry.getName().substring(0, 10));
			companyEntriesOfAllSwissAgencies.add(cEntry);
			TownEntry t = DatabaseService.getInstance().getTownById(cEntry.getTown());
			if(!mentionedTowns.contains(t)) {
				mentionedTowns.add(t);
			}
		}
		
		
		Map<TownEntry, Integer[]> townsWithNations = new HashMap<>();
		for(TownEntry tEntry : mentionedTowns) {
			System.out.println(tEntry.getName());
			// These are all records of a year which represent the swiss
			for(CompanyEntry cEntry : companyEntriesOfAllSwissAgencies) {
				// If the record is in the current town
				if(cEntry.getTown()==tEntry.getId()) {
					NormalizedCompanyEntry nCEntry = DatabaseService.getInstance().getNCompanyById(cEntry.getRepresents());
					NationEntry nEntry = DatabaseService.getInstance().getNationById(nCEntry.getNationality());
					if(nEntry.getId()==6) {
						System.out.println("-->Company "+nCEntry.getName()+" ["+nCEntry.getId()+"] ("+nEntry.getName()+")");
					}
					
					if(!townsWithNations.containsKey(tEntry)) {
						Integer[] newIntArray = new Integer[30];
						for(int i=0;i<30;i++) {
							newIntArray[i] = 0;
						}
						newIntArray[nEntry.getId()]++;
						townsWithNations.put(tEntry, newIntArray);
					}
					else {
						Integer[] current = townsWithNations.get(tEntry);
						current[nEntry.getId()]++;
					}
				}
			}
			
		}
		
		for (Map.Entry<TownEntry, Integer[]> entry : townsWithNations.entrySet()) {
			TownEntry key = entry.getKey();
			Integer[] value = entry.getValue();
			System.out.print(key.getName()+": ");
			for(int i=0;i<(30-key.getName().length());i++) {
				System.out.print(" ");
			}
			System.out.print("|");
			for(Integer i : value) {
				System.out.print(i+"|");
			}
			System.out.println();
		}
		
		String csvLine = "";
		for(TownEntry tEntry : mentionedTowns) {
			Integer[] numNations = townsWithNations.get(tEntry);
			csvLine += tEntry.getName()+";"+tEntry.getLat()+";"+tEntry.getLon()+";";
			csvLine += numNations[1]+";";	// Switzerland
			csvLine += numNations[2]+";";	// Germany
			csvLine += numNations[3]+";";	// Britain
			csvLine += numNations[20]+";";	// Netherlands
			csvLine += numNations[14]+";";	// United States
			
//			csvLine += numNations[4]+";";	// France
//			csvLine += numNations[21]+";";	// Not Assignable
//			csvLine += numNations[28]+";";	// Multinational
			csvLine += (numNations[4]+numNations[21]+numNations[28])+";";	// Various
			csvLine += (numNations[1]+numNations[2]+numNations[3]+numNations[20]+numNations[14]+numNations[4]+numNations[21]+numNations[28]);
			csvLine += "\n";
		}
		System.out.print(csvLine);
	}
}
