package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompanyEntry implements Entry {

	public static final int STATE_UNTOUCHED       = 0;
	public static final int STATE_CORRECTED       = 1;
	public static final int STATE_PARSED_AGENCIES = 2;
	public static final int STATE_PARSED_PERSONS  = 4;
	
	private int id;
	private int issue;
	private int page;
	private int pagePdf;
	private int town;
	private int type;
	private int state;
	private int represents;
	private String originalText;
	
	public CompanyEntry() {
		id             = 0;
		issue          = 0;
		page           = 0;
		pagePdf       = 0;
		town           = 0;
		type           = 0;
		state          = 0;
		represents     = 0;
		originalText  = "";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id             = resultSet.getInt(1);
		issue          = resultSet.getInt(2);
		page           = resultSet.getInt(3);
		pagePdf       = resultSet.getInt(4);
		town           = resultSet.getInt(5);
		type           = resultSet.getInt(6);
		state          = resultSet.getInt(7);
		represents     = resultSet.getInt(8);
		originalText  = resultSet.getString(9);
	}
	
	public int getId() {
		return id;
	}
	
	public int getIssue() {
		return issue;
	}
	
	public int getTown() {
		return town;
	}
	
	public String getName() {
		return originalText;
	}
	
	public void setName(String text) {
		originalText = text;
	}
	
	public String toString() {
		return originalText.substring(0, 10);
	}

	public int getRepresents() {
		return represents;
	}
	
	public int getState() {
		return state;
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	public String getEntryBlockFromOriginalText() {
		String[] agencyStrings = {"Agencies", "Agency", "General Managers", "Representatives of", "Agents for", "Representing", "Agences", "Agents"};
		for(String agencyString : agencyStrings) {
			if(originalText.contains(agencyString)) {
				return originalText.substring(originalText.lastIndexOf(agencyString) + agencyString.length()+1);
			}
		}
		return "No Agency String found!";
	}

	public String[] getEntryLines() {
		String[] agencyLines = getEntryBlockFromOriginalText().split("\n");
		List<String> cleanAgencyLines = new ArrayList<String>(); 
		for(String line : agencyLines) {
			line = line.trim();
			
			// Do not add [PAGEBREAK]
			if(!line.isEmpty() && 
					!line.contains("[PAGEBREAK]") && 
					!line.startsWith(">>")
					) {
				cleanAgencyLines.add(line);
			}
		}
		return cleanAgencyLines.toArray(new String[cleanAgencyLines.size()]);
	}

	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `company_record` (ISSUE, PAGE, PAGE_PDF, TOWN, TYPE, STATE, REPRESENTS, ORIGINAL_TEXT) values (?, ?, ?, ?, ?, ?, ?, ?)");
			preparedStmt.setInt(1, issue);
			preparedStmt.setInt(2, page);
			preparedStmt.setInt(3, pagePdf);
			preparedStmt.setInt(4, town);
			preparedStmt.setInt(5, type);
			preparedStmt.setInt(6, state);
			preparedStmt.setInt(7, represents);
			preparedStmt.setString(8, originalText);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `company_record` SET ISSUE=?, PAGE=?, PAGE_PDF=?, TOWN=?, TYPE=?, STATE=?, REPRESENTS=?, ORIGINAL_TEXT=? WHERE ID = ?");
			preparedStmt.setInt(1, issue);
			preparedStmt.setInt(2, page);
			preparedStmt.setInt(3, pagePdf);
			preparedStmt.setInt(4, town);
			preparedStmt.setInt(5, type);
			preparedStmt.setInt(6, state);
			preparedStmt.setInt(7, represents);
			preparedStmt.setString(8, originalText);
			preparedStmt.setInt(9, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `company_record` ORDER BY `ISSUE`, `PAGE` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `company_record` WHERE "+whereClause+" ORDER BY `ISSUE`, `PAGE` ASC";
	}
	
	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `company_record` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof CompanyEntry) {
			if(((CompanyEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public int compareTo(Entry o) {
		return originalText.compareTo(o.getName());
	}
	
}
