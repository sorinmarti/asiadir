package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AgencyEntry implements Entry {

	private int id;
	private int issue;
	private int town;
	private int companyRecord;
	private int represents;
	private String originalText;

	public AgencyEntry() {
		id = 0;
		issue = 0;
		town = 0;
		companyRecord = 0;
		represents     = 0;
		originalText  = "";
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id             = resultSet.getInt(1);
		issue          = resultSet.getInt(2);
		town           = resultSet.getInt(3);
		companyRecord  = resultSet.getInt(4);
		represents     = resultSet.getInt(5);
		originalText   = resultSet.getString(6);
	}
	
	public int getId() {
		return id;
	}
	
	public int getIssue() {
		return issue;
	}
	
	public void setIssue(int issue) {
		this.issue = issue;
	}
	
	public int getTown() {
		return town;
	}
	
	public void setTown(int town) {
		this.town = town;
	}
	
	public String getName() {
		return originalText;
	}
	
	public void setName(String name) {
		originalText = name;
	}
	
	public String toString() {
		return originalText + " ["+id+"]";
	}
	
	public int getRepresents() {
		return represents;
	}
	
	public void setRepresents(int repr) {
		represents = repr;
	}
	
	public int getCompanyRecord() {
		return companyRecord;
	}
	
	public void setCompanyRecord(int record) {
		companyRecord = record;
	}
	
	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `agency_record` (ISSUE, TOWN, COMPANY_RECORD, REPRESENTS, ORIGINAL_TEXT) values (?, ?, ?, ?, ?)");
			preparedStmt.setInt(1, issue);
			preparedStmt.setInt(2, town);
			preparedStmt.setInt(3, companyRecord);
			preparedStmt.setInt(4, represents);
			preparedStmt.setString(5, originalText);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `agency_record` SET ISSUE=?, TOWN=?, COMPANY_RECORD=?, REPRESENTS=?, ORIGINAL_TEXT=? WHERE ID = ?");
			preparedStmt.setInt(1, issue);
			preparedStmt.setInt(2, town);
			preparedStmt.setInt(3, companyRecord);
			preparedStmt.setInt(4, represents);
			preparedStmt.setString(5, originalText);
			preparedStmt.setInt(6, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `agency_record` ORDER BY `ORIGINAL_TEXT` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `agency_record` WHERE "+whereClause+" ORDER BY `ORIGINAL_TEXT` ASC";
	}
	
	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `agency_record` WHERE ID="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof AgencyEntry) {
			if(((AgencyEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public int compareTo(Entry o) {
		return originalText.compareTo(o.getName());
	}
	
}
