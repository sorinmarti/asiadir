package org.unibas.asiadir.db.entries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class IssueEntry implements Entry {

	private int id;
	private String name;
	private int pages;
	
	public IssueEntry() {
		id = 0;
		name = "";
		pages = 0;
	}
	
	public void fillEntry(ResultSet resultSet) throws SQLException {
		id   = resultSet.getInt(1);
		name = resultSet.getString(2);
		pages = resultSet.getInt(3);
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getPages() {
		return pages;
	}
	
	public String toString() {
		return String.valueOf(id);
	}

	@Override
	public PreparedStatement getSaveSQL(Connection conn) throws SQLException {
		PreparedStatement preparedStmt = null;
		if(id==0) {
			preparedStmt = conn.prepareStatement("INSERT INTO `issue` (YEAR, NAME, PAGES) values (?, ?, ?)");
			preparedStmt.setInt(1, id);
			preparedStmt.setString(2, name);
			preparedStmt.setInt(2, pages);
		}
		else {
			preparedStmt = conn.prepareStatement("UPDATE `issue` SET NAME=?, PAGES=? where YEAR = ?");
			preparedStmt.setString(1, name);
			preparedStmt.setInt(2, pages);
			preparedStmt.setInt(3, id);
		}
		return preparedStmt;
	}

	@Override
	public String getSelectSQL() {
		return "SELECT * FROM `issue` ORDER BY `NAME` ASC";
	}
	
	@Override
	public String getSelectSQL(String whereClause) {
		return "SELECT * FROM `issue` WHERE "+whereClause+" ORDER BY `NAME` ASC";
	}

	@Override
	public String getByIdSQL(int id) {
		return "SELECT * FROM `issue` WHERE YEAR="+id;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o==null) {
			return false;
		}
		
		if(o instanceof IssueEntry) {
			if(((IssueEntry)o).getId()==id) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public int compareTo(Entry o) {
		return name.compareTo(o.getName());
	}
}
