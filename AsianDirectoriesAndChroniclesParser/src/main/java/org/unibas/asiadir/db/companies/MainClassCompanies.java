package org.unibas.asiadir.db.companies;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.unibas.asiadir.db.DatabaseService;
import org.unibas.asiadir.db.entries.AgencyEntry;
import org.unibas.asiadir.db.entries.CompanyEntry;

public class MainClassCompanies {

	public static void main(String[] args) {
		
		Set<CompanyEntry> entries;
		
		// Connect to db & Retrieve list of all company objects
		entries = DatabaseService.getInstance().getCompanyEntries();
		List<CompanyEntry> filteredEntries = new ArrayList<>();
		entries.forEach( e -> { if(e.getState()==0 ) { filteredEntries.add(e); } } );
		
		// Create frame, show first entry
		DataManipulationForm form = new DataManipulationForm(filteredEntries);
		form.showNextEntry();
		form.setVisible(true);
		
	}
	
	 public static void updateCompanyState(CompanyEntry entry) throws ClassNotFoundException, SQLException {
		 entry.setState(CompanyEntry.STATE_PARSED_AGENCIES);
		 System.out.println("updateCompanyState: "+entry.getId());
		 DatabaseService.getInstance().updateCompanyEntry(entry);
	  }
	 
	 public static void createAgencyRecords(CompanyEntry entry, List<String> agencies) throws ClassNotFoundException, SQLException {
		 
		 for(String agency : agencies) {
			 agency = agency.trim();
			 if(agency.isEmpty()) {
				 System.err.println("Skipped empty line");
			 }
			 else {
				 AgencyEntry agencyObj = new AgencyEntry();
				 agencyObj.setName(agency);
				 agencyObj.setCompanyRecord(entry.getId());
				 agencyObj.setRepresents(1);
				 System.out.println("createAgencyRecord: "+agency);
				 DatabaseService.getInstance().createAgencyEntry(agencyObj);
			 }
		 }
	  }

}
