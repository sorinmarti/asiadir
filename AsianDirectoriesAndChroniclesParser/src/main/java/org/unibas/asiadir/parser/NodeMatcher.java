package org.unibas.asiadir.parser;

import org.w3c.dom.Node;

public interface NodeMatcher {
	boolean matches(Node node, Node[] attributes);
	void processMatch(Node node);
}
