package org.unibas.asiadir.parser;

import org.unibas.asiadir.SearchUtils;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CategoryParser {

	public static void parseNodes(NodeList nodeList, NodeMatcher... matchCriterias) {
		
		// Walk through every child node
		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);
			
			// If the node is an element: process it (ignore comments, CDATA, ...)
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				
				// If the node matches the provided name:  process this node
				if(tempNode.getNodeName().contains("text:span")) {
					for(NodeMatcher criteria : matchCriterias) {
						
						NamedNodeMap nodeMap = tempNode.getAttributes();
						Node[] attributes = new Node[nodeMap.getLength()];
						for (int i = 0; i < nodeMap.getLength(); i++) {
							attributes[i] = nodeMap.item(i);
						}
						
						if(criteria.matches(tempNode, attributes)) {
							criteria.processMatch(tempNode);
						}
					}	
				}
				
				// If the node has children: call this method
				// This iterates recursively through all elements
				if(tempNode.hasChildNodes()) {
					parseNodes(tempNode.getChildNodes(), matchCriterias);
				}
			}
		}
	}
}
