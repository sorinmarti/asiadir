package org.unibas.asiadir.parser;

import org.unibas.asiadir.SearchUtils;
import org.w3c.dom.Node;

public class MatcherFactory {

	public static NodeMatcher createSimpleSystemOutNodeMatcher(String prefix, String... stylesToSearch) {
		return new NodeMatcher() {
			
			@Override
			public void processMatch(Node node) {
				System.out.println(prefix + node.getTextContent());
			}
			
			@Override
			public boolean matches(Node node, Node[] attributes) {
				for(Node attrNode : attributes) {
					if(attrNode.getNodeName().contains("text:style-name") && SearchUtils.matchesOneSearchCriteria(attrNode.getNodeValue(), stylesToSearch)) {
						return true;
					}
				}
				return false;
			}
		};
	}
	
	public static NodeMatcher createSimpleSystemOutNumericNodeMatcher(String prefix, String... stylesToSearch) {
		return new NodeMatcher() {
			
			@Override
			public void processMatch(Node node) {
				System.out.println(prefix + node.getTextContent());
			}
			
			@Override
			public boolean matches(Node node, Node[] attributes) {
				for(Node attrNode : attributes) {
					if(attrNode.getNodeName().contains("text:style-name") && SearchUtils.matchesOneSearchCriteria(attrNode.getNodeValue(), stylesToSearch)) {

						String cleanedNodeText = SearchUtils.replaceAll(node.getTextContent(),"'","");

						return SearchUtils.isNumeric(cleanedNodeText);
					}
				}
				return false;
			}
		};
	}
}
