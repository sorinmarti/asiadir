package org.unibas.asiadir;

import java.io.File;

import org.junit.Test;
import org.unibas.asiadir.parser.CategoryParser;
import org.w3c.dom.Document;

public class MultipleFileParsingTest {

	private static final String[] FILES_TO_TEST = {
			"/content_1863.xml",
			"/content_1873.xml",
			"/content_1903.xml",
			"/content_1913.xml",
			"/content_1823.xml",
			"/content_1834.xml"};
	
	@Test
	public void testFindInMultipleDocuments() {
		for(String fileToParse : FILES_TO_TEST) {
			File file = FileHandler.getFileFromResources(fileToParse);
			Document doc = FileHandler.getDocumentFromFile(file);
		
			String elementNameToFind = "text:span";
			//CategoryParser.printNodes(doc.getChildNodes(), elementNameToFind, new String[]{}, new String[]{});
		}
		
	}
}
