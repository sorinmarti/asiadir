package org.unibas.asiadir;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;

import org.junit.Test;
import org.unibas.asiadir.parser.CategoryParser;
import org.unibas.asiadir.parser.MatcherFactory;
import org.unibas.asiadir.parser.NodeMatcher;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class FirstParsingTest {

	//private static final String FILE_TO_TEST = "/dummy.xml";
	private static final String FILE_TO_TEST = "/content_1863.xml";
	
	/**
	 * Simple test to determine if the resources
	 * directory can be accessed.
	 */
	@Test
	public void testFileOpen() {
		File file = FileHandler.getFileFromResources(FILE_TO_TEST);
		assertNotNull(file);
		
	}
	
	/**
	 * Test if a xml file can be opened and parsed.
	 */
	@Test
	public void testCreateDocument() {
		File file = FileHandler.getFileFromResources(FILE_TO_TEST);
		Document doc = FileHandler.getDocumentFromFile(file);
		assertNotNull(doc);
		
		File file2 = FileHandler.getFileFromResources("/nonexistent.xml");
		Document doc2 = FileHandler.getDocumentFromFile(file2);
		assertNull(doc2);	
	}
	
	@Test
	public void testFindInDocument() {
		File file = FileHandler.getFileFromResources(FILE_TO_TEST);
		Document doc = FileHandler.getDocumentFromFile(file);
		
		/*
		// 1903
		CategoryParser.parseNodes(doc.getChildNodes(), 
				MatcherFactory.createSimpleSystemOutNodeMatcher("TITLE: ", "CharStyle73"),
				MatcherFactory.createSimpleSystemOutNumericNodeMatcher("PAGE: ", "CharStyle43"));
		*/
		/*
		// 1873
		CategoryParser.parseNodes(doc.getChildNodes(), 
				 MatcherFactory.createSimpleSystemOutNodeMatcher("TITLE: ", "CharStyle43"),
				 MatcherFactory.createSimpleSystemOutNumericNodeMatcher("PAGE: ", "CharStyle57", "T65", "CharStyle66", "CharStyle57", "T95"));
		*/
		
		// 1863
//		CategoryParser.parseNodes(doc.getChildNodes(), 										
//				MatcherFactory.createSimpleSystemOutNodeMatcher("TITLE: ", "CharStyle33", "CharStyle34"),
//				MatcherFactory.createSimpleSystemOutNumericNodeMatcher("PAGE: ",  "T2", "T24", "CharStyle6","CharStyle90"));
		
		CategoryParser.parseNodes(doc.getChildNodes(), 										
				MatcherFactory.createSimpleSystemOutNodeMatcher("", "CharStyle33", "CharStyle34"));
	}
	
	
}
